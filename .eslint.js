module.exports = {
  root: true,
  env: {
    node: true,
    JWT_SECRET: "asdasdasdad",
  },
  'extends': [
    'eslint:recommended',
  ],
  parserOptions: {
    parser: '@typescript-eslint/parser'
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-unused-vars': 'warn',
    'no-global-assign': 'warn',
    'no-case-declarations': 'warn',
  }
}
