import nodemailer from 'nodemailer'
import MailLog from '../models/MailLog'

export default class MailService{
  mailConfig:any
  transporter:any

  constructor(){
    this.mailConfig = {
      host:process.env.EMAIL_HOST,
      port: process.env.EMAIL_PORT,
      auth: {
        user: process.env.EMAIL_USER,
        pass: process.env.EMAIL_PASSWORD
      }
    }
    this.transporter = nodemailer.createTransport(this.mailConfig)
  }

  sendEmail(to, subject, html, text){

    // console.log('in mailer')

    var message = {
      from: `${this.mailConfig.user}`,
      to: `${to}`,
      subject: `${subject}`,
      text: `${text}`,
      html: `${html}`
    }

    try{
      this.transporter.sendMail(message, function(err, info){
        if(err)
          console.log(err)
        else
          return info
      })
    }catch(e){
      let mailLog = new MailLog({
        time: new Date(),
        to:message.to,
        subject: message.subject,
        text: message.text,
      })
      mailLog.save()
    }
  }
}
