export default{
  base_url: process.env.VUE_APP_API_URL,
  api_key: process.env.VUE_APP_API_KEY,
  auth_url: process.env.VUE_APP_AUTH_URL,
  env: process.env.NODE_ENV,
}