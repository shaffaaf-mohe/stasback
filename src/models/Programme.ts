import mongoose, { Schema} from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'


export interface IProgramme extends mongoose.Document{
  _id  : string
  name : string
  field: string
  level: string
}


export const ProgrammeSchema : Schema = new Schema({
  name  : {type : String                 , required: true , index: true     , unique: false}        ,
  field : {type  : Schema.Types.ObjectId , ref : 'Field'  , required : true , autopopulate : true} ,
  level : {type  : String                , index: true    , required: true  , }                    ,
  normalizedName : {
    type : String,
    required : true,
    index   : true,
    unique : true,
    default: function(){
      return (this.field + this.level + this.name).replace(/[^\w\s]|_/g, "").replace(/\s+/g, " ").toLowerCase()
    }
  },
})

ProgrammeSchema.plugin(uniqueValidator)
ProgrammeSchema.plugin(require('mongoose-autopopulate'))

export default mongoose.model<IProgramme>('Programme', ProgrammeSchema)
