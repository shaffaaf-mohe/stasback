import mongoose, { Schema} from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'
import { IPerson } from './Person'
import Statistics from './Statistics'
import { EInstituteType } from './Statistics'
import User, { UserType, UserStatus } from './User'
import { v4 as uuidv4 } from 'uuid';


export interface ICampus extends mongoose.Document{
  name               : string
  isGovt             : boolean
  isPrimary ?        : boolean
  registrationNumber : string
  email ?            : string
  address ?          : string
  atoll              : string
  island             : string
  primaryContact     : IPerson
  ownerId            : string
  statsComplete      : boolean
  customEndDate      : Date
  completedPercent: number
}


export const CampusSchema : Schema = new Schema({
  name: {type: String, required: true, index:true },
  isGovt: {type: Boolean, required: true, default:false},
  isPrimary : {type: Boolean, required: true, default:false},
  registrationNumber : {type: String, required: false, unique:true},
  email : {type: String, required: false, unique:false},
  address: {type: String, required: false, unique:false},
  atoll : {type: String, required: false, unique:false},
  island : {type: String, required: false, unique:false},
  primaryContact : {type: Schema.Types.ObjectId, ref:'Person', required:false, autopopulate: true},
  ownerId: {type: Schema.Types.ObjectId, ref:'Institution', required:true, autopopulate: true},
  statsComplete: {type: Boolean, required: true, default:false},
  customEndDate: {type: Date, required: false, default:null},
  completedPercent: { type : Number, required: true, default:0  },
})

CampusSchema.plugin(uniqueValidator)
CampusSchema.plugin(require('mongoose-autopopulate'))

CampusSchema.post('save', async function(doc, next){
  const institution = doc
  let statistics = await Statistics.findOne({ relatedInstitution: institution._id, institutionType: EInstituteType.Campus })
  if(!statistics){
    statistics = new Statistics({
      relatedInstitution: institution._id,
      institutionType: EInstituteType.Campus,
    })
    statistics.save()
  }
  let associatedUser = await User.findOne({ relatedHEI: institution._id })
  if(!associatedUser){
    const uuid = uuidv4()
    let associatedUser = new User({
      name       : institution.name,
      email      : institution.email,
      type       : UserType.HEIStaff,
      status     : UserStatus.AutoCreated,
      relatedHEI : institution._id,
      heiType    : 'Campus',
      inviteId   :  mongoose.Types.ObjectId(),
    })
    await associatedUser.save()
  }
  next()
})


export default mongoose.model<ICampus>('Campus', CampusSchema)
