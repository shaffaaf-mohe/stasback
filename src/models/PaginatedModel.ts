import {Document} from 'mongoose'

export default class PaginatedModel{
  constructor(data:Array<Document>, count: number,  skipped: number, limit: number){
    this.data = data
    this.setPage(count, skipped, limit)
  } 
  data: Array<Document>
  page: Page
  
  setPage = (count:number, skipped:number, limit:number) => {
    let totalPages = Math.trunc(count/limit) + (count%limit == 0 ? 0 : 1 )
    let currentPage = Math.trunc(skipped/limit)+1
    this.page = new Page(currentPage, limit, count ,totalPages) 
  }

  getPaginatedModel = () => {
    return {data: this.data, page: this.page}
  }
}

export class Page{
  constructor(page:number, limit:number, totalItems:number, totalPages:number)
  {
    this.page = page 
    this.limit = limit
    this.totalItems = totalItems
    this.totalPages = totalPages
  }
  
  page: number
  limit: number
  totalPages: number
  totalItems: number
  
  getPage = () => {
    return {page: this.page, limit: this.limit, totalPages: this.totalPages, totalItems: this.totalItems}
  }
}