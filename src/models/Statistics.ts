import mongoose, { Schema} from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'
import {enumToArray} from '../utils/utilFuncs'
import { IProgramme } from './Programme'

export enum EInstituteType {
  Institution = 'Institution',
  Campus = 'Campus',
  OutreachCenter = 'OutreachCenter',
}

export interface IStatistics extends mongoose.Document{
  relatedInstitution : string
  institutionType   : EInstituteType
  lecturers       : ILecturerStats
  staff           : IStaffStats

  programmeStats  : Array<IProgrammeStatistics>
  completed       : boolean
  completedPercent: number

  startDate : Date
  endDate   : Date
}

export interface ILecturerStats extends mongoose.Document {
  fullTimeForeignMale   : number
  fullTimeForeignFemale : number
  fullTimeLocalMale     : number
  fullTimeLocalFemale   : number

  partTimeForeignMale   : number
  partTimeForeignFemale : number
  partTimeLocalMale     : number
  partTimeLocalFemale   : number
}

export interface IStaffStats extends mongoose.Document {
  male   : number
  female : number
}

export interface IProgrammeStatistics {
  _id         ?: string
  programme   : IProgramme
  maleStats   : IGenderStats
  femaleStats : IGenderStats
  completed   : boolean
}

export interface IGenderStats {
  newStudents : number
  oldStudents : number
  dropouts    : number
  graduates   : number
}

export const LecturerStats : Schema = new Schema({
  fullTimeForeignMale   :{type:Number , },
  fullTimeForeignFemale :{type:Number , },
  fullTimeLocalMale     :{type:Number , },
  fullTimeLocalFemale   :{type:Number , },

  partTimeForeignMale   :{type:Number , },
  partTimeForeignFemale :{type:Number , },
  partTimeLocalMale     :{type:Number , },
  partTimeLocalFemale   :{type:Number , },
})

export const StaffStats : Schema = new Schema({
  male   :{type:Number , },
  female :{type:Number , },
})


export const GenderStats : Schema = new Schema({
  newStudents :{type:Number , },
  oldStudents :{type:Number , },
  dropouts    :{type:Number , },
  graduates   :{type:Number , },
})

export const ProgrammeStats : Schema = new Schema({
  programme   : {type: Schema.Types.ObjectId, ref: 'Programme', required: true, autopopulate: true},
  maleStats   :{type:GenderStats , },
  femaleStats :{type:GenderStats , },
  completed   : {type: Boolean, default: false},
})

export const Statistics : Schema = new Schema({
  relatedInstitution: {type: Schema.Types.ObjectId, refPath:'institutionType', required:true, autopopulate: true},
  institutionType: {type:String, required: true, enum : enumToArray(EInstituteType)},

  lecturers      : { type : LecturerStats  },
  staff          : { type : StaffStats  },

  programmeStats  : { type : [ProgrammeStats]  },
  completed       : { type : Boolean, required: true, default:false  },
  completedPercent: { type : Number, required: true, default:0  },

  normalizedName : {
    type : String,
    required : true,
    index   : true,
    unique : true,
      default: function(){
      return (this.relatedInstitution + this.institutionType).replace(/[^\w\s]|_/g, "").replace(/\s+/g, " ").toLowerCase()
    }
  },

  startDate: { type : Date, required: false,   },
  endDate  : { type : Date, required: false,   },
})

Statistics.plugin(uniqueValidator)
Statistics.plugin(require('mongoose-autopopulate'))

export default mongoose.model<IStatistics>('Statistics', Statistics)
