import mongoose, { Schema} from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'
import {enumToArray} from '../utils/utilFuncs'

export interface IMinistrySection extends mongoose.Document{
  name               : string
  email ?            : string
  customEndDate      : Date
}


export const MinistrySectionSchema : Schema = new Schema({
  name: {type: String, required: true, index:true, unique: true},
  email: {type: String, required: false, unique:false},
  customEndDate: {type: Date, required: false, default:null},
})

MinistrySectionSchema.plugin(uniqueValidator)
MinistrySectionSchema.plugin(require('mongoose-autopopulate'))

export default mongoose.model<IMinistrySection>('MinistrySection', MinistrySectionSchema)
