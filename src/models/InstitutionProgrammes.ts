import mongoose, { Mongoose, Schema} from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'
import { IProgramme,  } from './Programme'
import { enumToArray } from '../utils/utilFuncs'

export enum EInstituteType {
  Institution = 'Institution',
  Campus = 'Campus',
  OutreachCenter = 'OutreachCenter',
}

export interface IInstitutionProgramme extends mongoose.Document{
  relatedInstitution : string
  institutionType   : EInstituteType

  programmes     : Array<string> | IProgramme
}


export const InstitutionProgrammeSchema : Schema = new Schema({
  relatedInstitution: {type: Schema.Types.ObjectId, refPath:'instituteType', required:true, autopopulate: true},
  institutionType: {type:String, required: true, enum : enumToArray(EInstituteType)},

  programmes: { type: [Schema.Types.ObjectId], ref: 'Programme', autopopulate: true },
})

InstitutionProgrammeSchema.plugin(uniqueValidator)
InstitutionProgrammeSchema.plugin(require('mongoose-autopopulate'))

export default mongoose.model<IInstitutionProgramme>('InstitutionProgramme', InstitutionProgrammeSchema )
