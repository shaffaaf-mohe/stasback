import mongoose, { Schema} from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'


export interface IMainField extends mongoose.Document{
  name : string
  normalizedName : string
}


export const MainFieldSchema : Schema = new Schema({
  name: { type : String, required : true, index   : true, unique : true},
  normalizedName : {
    type : String,
    required : true,
    index   : true,
    unique : true,
      default: function(){
      return this.name.replace(/[^\w\s]|_/g, "").replace(/\s+/g, " ").toLowerCase()
    }},
})

MainFieldSchema.plugin(uniqueValidator)
MainFieldSchema.plugin(require('mongoose-autopopulate'))

export default mongoose.model<IMainField>('MainField', MainFieldSchema)
