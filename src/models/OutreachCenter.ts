import mongoose, { Schema} from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'
import Statistics from './Statistics'
import { EInstituteType } from './Statistics'


export interface IOutreachCenter extends mongoose.Document{
  name               : string
  isGovt             : boolean
  registrationNumber : string
  address ?          : string
  atoll              : string
  island             : string
  ownerId            : string
  statsComplete      : boolean
  completedPercent: number
}


export const OutreachCenterSchema : Schema = new Schema({
  name:                { type: String, required: true, index:true, unique: true},
  isGovt:              { type: Boolean, required: true, default:false},
  registrationNumber : { type: String, required: false, unique:true},
  address:             { type: String, required: false, },
  atoll :              { type: String, required: false, },
  island :             { type: String, required: false, },
  ownerId:             { type: Schema.Types.ObjectId, ref:'Institution', required:true, autopopulate: true},
  statsComplete: {type: Boolean, required: true, default:false},
  completedPercent: { type : Number, required: true, default:0  },
})

OutreachCenterSchema.plugin(uniqueValidator)
OutreachCenterSchema.plugin(require('mongoose-autopopulate'))

OutreachCenterSchema.post('save', async function(doc, next){
  const institution = doc
  let statistics = await Statistics.findOne({ relatedInstitution: institution._id, institutionType: EInstituteType.OutreachCenter })
  if(!statistics){
    statistics = new Statistics({
      relatedInstitution: institution._id,
      institutionType: EInstituteType.OutreachCenter,
    })
    statistics.save()
  }
  next()
})

export default mongoose.model<IOutreachCenter>('OutreachCenter', OutreachCenterSchema)
