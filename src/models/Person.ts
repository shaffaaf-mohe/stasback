import mongoose, { Schema} from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'
import {enumToArray} from '../utils/utilFuncs'

export interface IPerson extends mongoose.Document{
  name: string
  nid ?: string
  passport ?: string
  email?: string
  address ?: string
  country ?: string
  phoneNumber ?: string
  phoneNumber2 ?: string
}

export const PersonSchema : Schema = new Schema({
  name: {type: String, required: true, index:true},
  nid: {type: String, required: false, index:true, unique:true},
  passport: {type: String, required: false, index:true, unique: false},
  email: {type: String, required: false,  index:true},
  address: {type: String, required: false,  index:true},
  country: {type: String, required: false,  index:true},
  phoneNumber: {type:String, index:true},
  phoneNumber2: {type:String, index:true},
})

PersonSchema.plugin(uniqueValidator)

export default mongoose.model<IPerson>('Person', PersonSchema)
