import mongoose, { Schema} from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'
import {enumToArray} from '../utils/utilFuncs'
import { IPerson } from './Person'
import Statistics from './Statistics'
import { EInstituteType } from './Statistics'
import User, { UserType, UserStatus } from './User'
import { v4 as uuidv4 } from 'uuid';

export enum CollegeType {
  Academic = 'Academic',
  Technical = 'Technical',
}

export interface IInstitution extends mongoose.Document{
  name               : string
  isGovt             : boolean
  registrationNumber : string
  email ?            : string
  address ?          : string
  primaryContact     : IPerson
  type               : CollegeType
  statsComplete      : boolean
  programmes         : Array<string>
  completedPercent: number
}


export const InstitutionSchema : Schema = new Schema({
  name: {type: String, required: true, index:true, unique: true},
  isGovt: {type: Boolean, required: true, default:false},
  registrationNumber : {type: String, required: false, unique:true},
  email: {type: String, required: true, unique:false},
  address: {type: String, required: false, unique:false},
  atoll: {type: String, required: false, unique:false},
  island: {type: String, required: false, unique:false},
  primaryContact : {type: Schema.Types.ObjectId, ref:'Person', required:false, autopopulate: true},
  type: { type: String, enum: [...enumToArray(CollegeType)]},
  statsComplete: {type: Boolean, required: true, default:false},
  programmes: {type: [Schema.Types.ObjectId], required: false, default:[]},
  completedPercent: { type : Number, required: true, default:0  },
})

InstitutionSchema.plugin(uniqueValidator)
InstitutionSchema.plugin(require('mongoose-autopopulate'))

InstitutionSchema.post('save', async function(doc, next){
  const institution = doc
  let statistics = await Statistics.findOne({ relatedInstitution: institution._id, institutionType: EInstituteType.Institution })
  if(!statistics){
    statistics = new Statistics({
      relatedInstitution: institution._id,
      institutionType: EInstituteType.Institution,
    })
    statistics.save()
  }
  let associatedUser = await User.findOne({ relatedHEI: institution._id })
  if(!associatedUser){
    const uuid = uuidv4()
    let associatedUser = new User({
      name       : institution.name,
      email      : institution.email,
      type       : UserType.HEIStaff,
      status     : UserStatus.AutoCreated,
      relatedHEI : institution._id,
      heiType    : 'Institution',
      inviteId   :  new mongoose.Types.ObjectId(),
    })
    await associatedUser.save()
  }
  next()
})
export default mongoose.model<IInstitution>('Institution', InstitutionSchema)
