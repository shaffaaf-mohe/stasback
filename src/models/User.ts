import mongoose, { Mongoose, Schema} from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'
import {enumToArray} from '../utils/utilFuncs'
import { v4 as uuidv4 } from 'uuid';


export enum UserType {
  Admin='Admin',
  HEMStaff='HEMStaff',
  HEIStaff='HEIStaff',
}

export enum HEIType {
  Institution='Institution',
  Campus='Campus',
}

export enum UserStatus {
  AutoCreated = 'AutoCreated',
  AddRequested = 'AddRequested',
  AwaitPassword = 'AwaitPassword',
  Invited = 'Invited',
  Registered = 'Registered',
  RegisteredAwaitPwd = 'RegisteredAwaitPwd',
  Active = 'Active',
  Inactive = 'Inactive',
  Deleted = 'Deleted',
}

export enum UserConfirmationMethod{
  Email = 'Email',
  Management = 'Management',
}

export interface IUser extends mongoose.Document{
  name : string
  password: string
  email?: string
  type: UserType
  accessToken: string
  status ?: UserStatus
  relatedHEI ?: string
  heiType : HEIType
  inviteId ?: string
  userConfirmation ?: IUserConfirmation
  changeConfirmation ?: IChangeConfirmation
}

export interface IUserConfirmation {
  emailConfirmString: string
  confirmationMethod: UserConfirmationMethod
  confirmedBy: string
}

export enum UserChangeTypes{
  'email' = 'email',
}

export interface IUserChange{
  changeType: UserChangeTypes
  change: string
}

export interface IChangeConfirmation {
  emailConfirmString: string
  changes: Array<IUserChange>
  confirmedBy ?: string
}

export const UserChangeSchema : Schema = new Schema ({
  changeType: {type: String, enum: [...enumToArray(UserChangeTypes)]},
  change: {type:String}
})

export const ChangeConfirmationSchema: Schema = new Schema ({
  emailConfirmString: {type:String},
  changes: {type:[ UserChangeSchema ]},
  confirmedBy: {type: Schema.Types.ObjectId, ref: 'User'}
})

export const UserConfirmationSchema: Schema = new Schema ({
  emailConfirmString: {type:String},
  confirmationMethod: {type:String, enum: [...enumToArray(UserConfirmationMethod)]},
  confirmedBy: {type: Schema.Types.ObjectId, ref: 'User'}
})

export const UserSchema : Schema = new Schema({
  name: { type: String, required: true},
  relatedHEI: { type: Schema.Types.ObjectId, refPath: 'heiType', autopopulate:true },
  heiType: { type: String, enum : [ ...enumToArray(HEIType) ]},
  inviteId: {type: String, required: false},
  email: {type: String, required: false, unique:false},
  password: {type:String, required: false},
  type: {type:String, required: true, enum: [...enumToArray(UserType)], default:UserType.HEMStaff},
  accessToken :{type: String},
  status : {type: String, required:true, enum: [...enumToArray(UserStatus)], default:UserStatus.Inactive},
  userConfirmation: {type:UserConfirmationSchema},
  changeConfirmation: {type:ChangeConfirmationSchema},
})

UserSchema.plugin(uniqueValidator)
UserSchema.plugin(require('mongoose-autopopulate'))

export default mongoose.model<IUser>('User', UserSchema)
