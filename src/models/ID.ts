import mongoose, {Schema, Document} from 'mongoose'
import { enumToArray } from '../utils/utilFuncs'

export enum IDTypes {
  NationalId = 'NationalId',
  Passport = 'Passport',
  DriversLicense = 'DriversLicense',
}

export interface IIdentification {
  type : IDTypes
  number : string
}

export const IdentificationSchema : Schema = new Schema({
  type: {type:String, required:true, enum: [...enumToArray(IDTypes)]},
  number: {type:String, required:true },
})

export default mongoose.model<IIdentification>('Identification', IdentificationSchema)
