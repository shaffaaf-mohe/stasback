import mongoose, {Schema, Document} from 'mongoose'
import {enumToArray} from '../utils/utilFuncs'
import {IUser, UserSchema}  from '../models/User'
import {IActionLog, ActionLogSchema} from './ActionLog'

export enum RequestUserAddStatuses   {
  Requested = 'Requested',
  Approved = 'Approved',
  Rejected = 'Rejected',
}

export interface IRequestAddUser extends Document {
  requestBy : string
  user: IUser
  status: RequestUserAddStatuses
  actionLog: Array<IActionLog>
  addedUserId: string
}

export const RequestAddUserSchema : Schema = new Schema({
  requestBy : {type: Schema.Types.ObjectId, ref:'User', required:true, autopopulate:{select:'-password -accessToken'}},
  user: {type: UserSchema, required:true},
  status: {type:Schema.Types.String, enum:enumToArray(RequestUserAddStatuses), required:true},
  actionLog: {type: [ActionLogSchema], },
  addedUserId: {type: Schema.Types.ObjectId}
})

RequestAddUserSchema.plugin(require('mongoose-autopopulate'))
export default mongoose.model<IRequestAddUser>('RequestAddUser', RequestAddUserSchema)
