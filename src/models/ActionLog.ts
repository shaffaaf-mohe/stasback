import mongoose, {Schema, Document} from 'mongoose'
import {enumToArray} from '../utils/utilFuncs'
import {IUser, UserSchema}  from '../models/User'


export interface IActionLog extends Document {
  actionBy : string
  actionTaken: string
}

export const ActionLogSchema : Schema = new Schema({
  actionBy : {type: Schema.Types.ObjectId, ref:'User', refPath:'users', required:true, autopopulate:{select:'-password -accessToken'}},
  actionTaken: {type:Schema.Types.String,  required:true},
})

ActionLogSchema.plugin(require('mongoose-autopopulate'))
export default mongoose.model<IActionLog>('ActionLog', ActionLogSchema)
