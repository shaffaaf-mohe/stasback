import mongoose, { Schema} from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'
import {enumToArray} from '../utils/utilFuncs'
import ActionLog, {IActionLog, ActionLogSchema} from './ActionLog'



export interface IEntity extends mongoose.Document{
  name: string
  refNumber: string
  isGovt: boolean
  isInstitution: boolean
  email?: string
  address ?: string
  caseNumbers : Array<string>
  editHistory    : Array<IActionLog>
}


export const EntitySchema : Schema = new Schema({
  name: {type: String, requried: true, index:true},
  refNumber: {type: String, required: false, unique:true},
  email: {type: String, required: false, unique:false},
  address: {type: String, required: false, unique:false},
  caseNumbers: [{ type:String, index:true }],
  isGovt: { type:Boolean, index:true },
  isInstitution: { type:Boolean, index:true },
  editHistory: [{type:ActionLogSchema, required:true}],
})

EntitySchema.plugin(uniqueValidator)

export default mongoose.model<IEntity>('Entity', EntitySchema)
