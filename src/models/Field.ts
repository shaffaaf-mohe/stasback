import mongoose, { Schema} from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'


export interface IFieldSchema extends mongoose.Document{
  name : string
  mainField : string
}


export const FieldSchema : Schema = new Schema({
  name      : { type : String, required : true, index   : true, unique : true},
  mainField : {type: Schema.Types.ObjectId, ref:'MainField', required:true, autopopulate: true},
  normalizedName : {
    type : String,
    required : true,
    index   : true,
    unique : true,
      default: function(){
      return (this.mainField + this.name).replace(/[^\w\s]|_/g, "").replace(/\s+/g, " ").toLowerCase()
    }
  },
})

FieldSchema.plugin(uniqueValidator)
FieldSchema.plugin(require('mongoose-autopopulate'))

export default mongoose.model<IFieldSchema>('Field', FieldSchema)
