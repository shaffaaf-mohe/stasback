import mongoose, {Schema, Document} from 'mongoose'
import { enumToArray } from '../utils/utilFuncs'

export enum FileType{
  jpeg = 'jpg',
  png = 'png',
  pdf = 'pdf',
  docx = 'docx',
  csv = 'csv',
  generic = 'generic',
}

export interface IFileUpload{
  name: 'string',
  size: 'string',
  encoding: 'string'
  tempFilePath: 'string'
  truncated: 'string'
  mimetype: 'string'
}

export interface IFile extends Document {
  fileLocation: string
  fileType: string
}

const FileSchema : Schema = new Schema({
  fileLocation: {type:String, required:true},
  fileType: {type:String, required:true, enum:[...enumToArray(FileType)]}
})

export const getFileType = (mimeType:string) => {
  switch (mimeType) {
    case 'image/jpeg':
      return FileType.jpeg

    case 'image/png':
      return FileType.png

    case 'text/csv':
      return FileType.csv

      default:
      return FileType.generic
  }
}
export default mongoose.model<IFile>('File', FileSchema)
