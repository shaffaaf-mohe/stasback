import mongoose, { Schema} from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'


export interface IMailLog extends mongoose.Document{
  _id  : string
  time : Date
  to   : string
  subject : string
  text : string
}


export const MailLogSchema : Schema = new Schema({
  time  : { type : Date, required : true },
  to : {type  : String,  required : true, },
  subject : { type: String, required: true, },
  text : { type: String, required: false, },
})


export default mongoose.model<IMailLog>('MailLog', MailLogSchema)
