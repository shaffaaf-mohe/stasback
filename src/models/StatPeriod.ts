import mongoose, { Schema} from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'


export interface IStatPeriod extends mongoose.Document{
  _id  : string
  start : Date
  end : Date
}


export const StatPeriodSchema : Schema = new Schema({
  start  : { type : Date, required : true },
  end : {type  : Date,  required : true, },
})


export default mongoose.model<IStatPeriod>('StatPeriod', StatPeriodSchema)
