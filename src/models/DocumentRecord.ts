import mongoose, { Schema} from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'
import {enumToArray} from '../utils/utilFuncs'
import ActionLog, {IActionLog, ActionLogSchema} from './ActionLog'

enum BelongsToType {
  Person = 'Person',
  Institution  = 'Institution',
}

export interface ICase extends mongoose.Document {
  type : string
  ref  : string
}

export interface IDocument extends mongoose.Document{
  fileNumber     : string
  documentType   : string
  documentDate   : Date
  newFileName    : string
  oldFileName    : string
  originalStart  : number
  originalEnd    : number
  references     : Array<string>
  caseNumbers    : Array<ICase>
  relatedParties : Array<string>
  editHistory    : Array<IActionLog>
  notLast        : boolean
  verified       : boolean
  rejected       : boolean
  rejectReason   : string
}

export const CaseSchema : Schema = new Schema({
  type: {type:String, required:true, index:true},
  ref: {type:String, required:true, index:true},
})

export const DocumentSchema : Schema = new Schema({
  fileNumber: {type:String, required:false, index:true, },
  documentType: {type:String, required:true, index:true, },
  documentDate: {type:Date, required:false, index:true},
  newFileName: {type: String, required: true, index:true},
  oldFileName: {type: String, required: false, index:true},
  originalStart: { type: Number, required: true },
  originalEnd: { type: Number, required: true },
  references: [{type:String, required:false, index:true, }],
  caseNumbers: [{type:CaseSchema, required:false, index:true}],
  relatedParties: [{type:mongoose.Types.ObjectId, required:false, ref:'Entity', refPath:'entities', autopopulate: true}],
  editHistory: [{type:ActionLogSchema, required:true}],
  notLast: {type:Boolean, required:false},
  verified: {type: Boolean, required:false, default:false,},
  rejected: {type: Boolean, required:false, default:false,},
  rejectReason: {type: String, required: true, },
})

DocumentSchema.plugin(uniqueValidator)
DocumentSchema.plugin(require('mongoose-autopopulate'))
// DocumentSchema.index({loanApplicationId: 'text', 'documentRecords.name': 'text'})
export default mongoose.model<IDocument>('Document', DocumentSchema)
