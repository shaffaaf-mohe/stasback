import express, {Application, Request, Response} from 'express'
const router = express.Router();

import Auth, {Admin, Active, Sorter, Viewer} from '../middlewares/Auth'

import AuthController from '../controllers/AuthController'
import UserController from '../controllers/UserController'
// import {body, validationResult } from 'express-validator'
import PredefinedValuesController from '../controllers/PredefinedValuesController'
// import FileController  from '../controllers/FileController'
// import TestController from '../controllers/TestController'
// import DocumentsController from '../controllers/DocumentsController'
// import EntityController from '../controllers/EntityController'
import PeopleController from '../controllers/PeopleController'
import InstitutionController from '../controllers/InstitutionsController'
import CampusController from '../controllers/CampusController'
import OutreachCentersController from '../controllers/OutreachCentersController'
import MainFieldsController from '../controllers/MainFieldsController'
import FieldsController from '../controllers/FieldsController'
import ProgrammesController from '../controllers/ProgrammesController'
// import InstitutionProgrammesController from '../controllers/InstitutionProgrammesController'
import MailLogController from '../controllers/MailLogsController'

import StatPeriodController from '../controllers/StatPeriodController'
import StatisticsController from '../controllers/StatisticsController'

const statisticsController = new StatisticsController()

const statPeriodController = new StatPeriodController()
const authController = new AuthController()
const userController = new UserController()
// const fileController = new FileController()
// const testController = new TestController()
const predefinedValuesController = new PredefinedValuesController()
// const documentsController = new DocumentsController()
// const entityController = new EntityController()
const peopleController = new PeopleController()
const institutionController = new InstitutionController()
const campusController = new CampusController()
const outreachCentersController = new OutreachCentersController()
const mainFieldsController = new MainFieldsController()
const fieldsController = new FieldsController()
const programmesController = new ProgrammesController()
const mailLogsController = new MailLogController()
// const institutionProgrammesController = new InstitutionProgrammesController()

// //#region test routes
// router.post('/api/test', testController.test)
// router.post('/api/record', testController.record)
// //#endregion test routes
// //#region auth routes
// router.post('/api/auth/register', authController.register)
router.post('/api/auth/login', authController.login)
router.get('/api/auth/check', Auth,  authController.checkAuth )
// router.get('/api/auth/resendEmail', Auth, authController.resendEmail)
// //endregion auth routes

// #region user routes
router.get('/api/users', Auth,   userController.all)
router.get('/api/users/nopage', Auth,  userController.allNonPaged)
router.post('/api/users/:id/invite',Auth,  userController.sendInviteLinkA)
router.get('/api/users/:id',Auth, Admin, userController.single)
router.get('/api/users/byInvite/:id/', userController.singleByInvite)
router.post('/api/users/password/new/', userController.newPwdForInvitee)
router.patch('/api/users/:id',Auth, Admin, userController.update)
router.patch('/api/users/:id/ban',Auth, Admin, userController.ban)
router.patch('/api/users/:id/unban',Auth, Admin, userController.unban)
router.delete('/api/users/:id',Auth, Admin, userController.delete)
router.put('/api/users/:id/type/:type',Auth,  userController.changeType)
router.put('/api/users/:id/activate', Auth, Admin,  userController.activate)
router.put('/api/users/:id/deactivate', Auth, Admin,  userController.deactivate)
router.post('/api/users/:id/resetpwd', Auth, Admin, userController.resetPwd)
router.get('/api/users/activate/email/:string', userController.activateEmail)
router.put('/api/users/change',Auth, userController.change)
router.get('/api/users/change/email/:string', userController.changeEmail)
router.post('/api/users/change/password', Auth, userController.changePassword)
// #endregion user routes

// #region educational-institution routes
router.get('/api/mail-logs', Auth,  mailLogsController.all )
// #endregion
// #region educational-institution routes
router.get('/api/main-fields/downloadExample', Auth,  mainFieldsController.downloadExample )
router.post('/api/main-fields', Auth,   mainFieldsController.add )
router.post('/api/main-fields/upload', Auth,   mainFieldsController.uploadData )
router.patch('/api/main-fields/:id', Auth,   mainFieldsController.update )
router.get('/api/main-fields', Auth,    mainFieldsController.all )
router.get('/api/main-fields/:id', Auth,    mainFieldsController.single )
// #endregion
//
// #region educational-institution routes
router.get('/api/statsPeriod',  Auth, statPeriodController.current )
router.post('/api/statsPeriod',  Auth, statPeriodController.save )
// #endregion
// #region educational-institution routes
router.get('/api/fields/downloadExample',  Auth, fieldsController.downloadExample )
router.post('/api/fields', Auth,   fieldsController.add )
router.post('/api/fields/upload', Auth,   fieldsController.uploadData )
router.patch('/api/fields/:id', Auth,   fieldsController.update )
router.get('/api/fields', Auth,    fieldsController.all )
router.get('/api/fields/:id', Auth,    fieldsController.single )
// #endregion
//
// #region educational-institution routes
router.get('/api/programmes/downloadExample', Auth,   programmesController.downloadExample )
router.get('/api/programmes/getNonPaged', Auth,   programmesController.allNonPaged )
router.post('/api/programmes', Auth,   programmesController.add )
router.post('/api/programmes/upload', Auth,   programmesController.uploadData )
router.patch('/api/programmes/:id', Auth,   programmesController.update )
router.get('/api/programmes', Auth,    programmesController.all )
router.get('/api/programmes/:id', Auth,    programmesController.single )
// #endregion
//
// #region educational-institution routes
router.get('/api/educational-institutions/downloadExample', Auth,   institutionController.downloadExample )
router.get('/api/educational-institutions', Auth,    institutionController.all )
router.get('/api/educational-institutions/:id', Auth,    institutionController.single )
router.post('/api/educational-institutions/upload', Auth,   institutionController.uploadData )
router.post('/api/educational-institutions/:id/invite', Auth,   institutionController.sendInviteLinkA )
router.post('/api/educational-institutions', Auth,   institutionController.add )
router.patch('/api/educational-institutions/:id', Auth,   institutionController.update )
// #endregion

// #region educational-institution routes
router.get('/api/campuses/downloadExample', Auth,   campusController.downloadExample )
router.get('/api/campuses', Auth,    campusController.all )
router.get('/api/campuses/:id', Auth,    campusController.single )
router.post('/api/campuses', Auth,   campusController.add )
router.patch('/api/campuses/:id', Auth,   campusController.update )
router.post('/api/campuses/upload', Auth,   campusController.uploadData )
router.post('/api/campuses/:id/invite', Auth,   campusController.sendInviteLinkA )
// #endregion
//
// #region educational-institution routes
router.get('/api/outreach-centers/downloadExample', Auth,   outreachCentersController.downloadExample )
router.post('/api/outreach-centers', Auth,   outreachCentersController.add )
router.patch('/api/outreach-centers/:id', Auth,   outreachCentersController.update )
router.post('/api/outreach-centers/upload', Auth,   outreachCentersController.uploadData )
router.get('/api/outreach-centers', Auth,    outreachCentersController.all )
router.get('/api/outreach-centers/my',  Auth, outreachCentersController.my )
router.get('/api/outreach-centers/:id', Auth,    outreachCentersController.single )
// #endregion
//
// #region people routes
router.get('/api/people/downloadExample', Auth,   peopleController.downloadExample )
router.post('/api/people', Auth,   peopleController.add )
router.patch('/api/people/:id', Auth,   peopleController.update )
router.get('/api/people', Auth,    peopleController.all )
router.get('/api/people/:id', Auth,    peopleController.single )
router.post('/api/people/upload', Auth,   peopleController.uploadData )
// #endregion
//

// #region predefined values
router.get('/api/predefinedValues/userTypes',   predefinedValuesController.getUserTypes)
router.get('/api/predefinedValues/atolls',   predefinedValuesController.getAtolls)
router.get('/api/predefinedValues/islands',   predefinedValuesController.getAllIslands)
router.get('/api/predefinedValues/islands/:atoll',   predefinedValuesController.getAtollIslands)
// #endregion predefined values

// #region predefined values
// router.get('/api/statistics/progress', statisticsController.progress)
router.get('/api/statistics/getNonPaged', Auth,   statisticsController.allNonPaged )
router.patch('/api/statistics/dates', Auth,  statisticsController.saveDates)
router.post('/api/statistics', Auth,  statisticsController.save)
router.post('/api/statistics/upload/:relatedInstitution/:institutionType', Auth,  statisticsController.uploadStatistics)
router.get('/api/statistics/programmes/downloadCSVForProgrammeAddition/:relatedInstitution/:institutionType', Auth,  statisticsController.downloadCSVForProgrammeAddition)
router.get('/api/statistics/programmes/downloadCSVForStatistics/:relatedInstitution/:institutionType', Auth,  statisticsController.downloadCSVForStatistics)
router.post('/api/statistics/programmes/upload/:relatedInstitution/:institutionType', Auth,  statisticsController.uploadProgrammes)
router.get('/api/statistics/programmes/:relatedInstitution/:institutionType/:programme', Auth,  statisticsController.getProgramme)
router.get('/api/statistics/programmes/:relatedInstitution/:institutionType', Auth,  statisticsController.getAllProgrammes)
router.patch('/api/statistics/programme/remove', Auth,  statisticsController.removeProgramme)
router.patch('/api/statistics/programme', Auth,  statisticsController.saveProgramme)
router.get('/api/statistics/:id/:type', Auth,  statisticsController.single)
// //#region fileUploads
// router.post('/api/files', fileController.upload)
// router.get('/api/files/:id/download', fileController.download)
// //#endregion fileUploads


export default router
