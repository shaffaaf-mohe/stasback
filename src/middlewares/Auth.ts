import express from 'express'
import jwt from 'jsonwebtoken'
import User from '../models/User'
import { UserType } from '../models/User'

const verifyToken = async function (req, res, next ){
  const bearerHeader = req.headers['authorization']
  if(typeof bearerHeader !== 'undefined')
  {
    try{
      const bearer = bearerHeader.split(' ')
      const bearerToken = bearer[1]
      // console.log('bearer token', bearerToken)
      const authData  = await jwt.verify(bearerToken, process.env.JWT_SECRET)
      const user = await User.findById(authData.userId).select('-password -accessToken')
      // console.log('user', user)
      req.user = user
      req.authData = authData
      // console.log('authData', authData)
      next()
    }catch(e){
      res.status(500).send({'error': e})
      return
    }
  }else{
    res.status(403).send({'mssg':'Cant verify token'})
    return
  }
}

export const Active = async function(req,res,next){
  try{
    const user = req.user
    if(user.status == 'Active')
      next()
    else
      res.status(500).json({'mssg': 'User not Active'})
  }catch(e){
    res.status(500).json({'mssg':e})
  }
}

export const Admin = async function(req, res, next){
  try{
    const user = req.user
    if(user.type === 'Master' || user.type === 'Admin')
      next()
    else
      res.status(403).json({'mssg': 'Admin privildges required'})
  }catch(e){
    res.status(500).json({'error': e})
  }
}


export const HEMStaff = async function(req, res, next){
  try{
    const user = req.user
    if(user.type == UserType.HEMStaff)
      next()
    else
      res.status(403).json({'mssg': 'Have to be a ministry staff to access this.'})
  }catch(e){
    res.status(500).json({'error': e})
  }
}

export const Viewer = async function(req, res, next){
  try{
    const user = req.user
    if(user.type === 'Master' || user.type === 'Admin' || user.type === 'Sorter')
      next()
    else
      res.status(403).json({'mssg': 'Admin privildges required'})
  }catch(e){
    res.status(500).json({'error': e})
  }
}


export const Sorter = async function(req, res, next){
  try{
    const user = req.user
    if(user.type === 'Master' || user.type === 'Admin' || user.type === 'Sorter' || user.type === 'Viewer')
      next()
    else
      res.status(403).json({'mssg': 'Admin privildges required'})
  }catch(e){
    res.status(500).json({'error': e})
  }
}

export const Verifier = async function(req, res, next){
  try{
    const user = req.user
    if(user.type === 'Master' || user.type === 'Admin', user.type === 'Verifier' )
      next()
    else
      res.status(403).json({'mssg': 'Admin privildges required'})
  }catch(e){
    res.status(500).json({'error': e})
  }
}
/*   try { */
/*     let token  = req.token */
/*     let authData = await jwt.verify(token, 'secretKey') */
/*     let req.authData = authData */
/*     next() */
/*   }catch(e){ */
/*     res.sendStatus(403) */
/*   } */
/* } */

// exports.refresh = function(req,res,next){
//
// }

export default verifyToken
