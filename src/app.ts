require('dotenv').config();
import express, {Application, Request, Response} from 'express'
import mongoose from 'mongoose'
import jwt from 'jsonwebtoken'
import bodyParser from 'body-parser'
import User from './models/User'
import {enumToArray, enumToDispValues} from './utils/utilFuncs'
import cors from 'cors'
import fileUpload from 'express-fileupload'
import routes from './routes/routes'
import verifyToken from './middlewares/Auth'
import path from 'path'
import Entity from './models/Entity'
// const routes = require('./routes/routes.ts')
import { ensureHEMSStaff, ensureAdmin } from './controllers/AuthController'

const app: Application = express()

app.set('view engine', 'pug')
app.set('views', path.join(__dirname, './views'));

app.use(bodyParser.json());
app.use('/public', express.static(path.join(__dirname, 'public')))


const port: number = 3000
app.use(cors());
app.use(fileUpload({
  limits: {fileSize: 4 * 1024 * 1024}
}))

// mongoose.set('debug', true)
// mongoose.set("debug", (collectionName, method, query, doc) => {
//     console.log(`${collectionName}.${method}`, JSON.stringify(query), doc);
// });
app.get('/', function (req, res) {
  res.render('index.pug', { title: 'MOHE app' });
});

const connectDb = ()=>{
  return mongoose.connect('mongodb://localhost:27017/stats', {useNewUrlParser: true})
}

app.use('/', routes);

connectDb().then(async()=>{
  // let ministry = await Entity.findOne({name: 'Ministry of Higher Education'}).exec()
  // let mqa = await Entity.findOne({name: 'Maldives Qualification Authority'}).exec()
  // console.log('mqa', mqa)
  // if(!ministry){
  //   let entity = new Entity({name: 'Ministry of Higher Education', isGovt:true, isInstitution:true, refNumber: 'MoHE'})
  //   await entity.save()
  // }
  // if(!mqa){
  //   let entity = new Entity({name: 'Maldives Qualification Authority', isGovt:true, isInstitution:true, refNumber: 'MQA'})
  //   await entity.save()
  // }
  ensureAdmin()
  ensureHEMSStaff()
  app.listen(port, () => {
    console.log('process.env', process.env.JWT_SECRET)
    console.log('process.env', process.env.SORTED_FILES_LOCATION)
    return console.log(`server is listening on ${port}`)
  });
})

