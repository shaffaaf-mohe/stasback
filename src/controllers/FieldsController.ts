import PaginatedModel from '../models/PaginatedModel'
import Field from '../models/Field'
import MainField from '../models/MainField'
import {parse} from 'csv-parse'
//@ts-ignore
import { getFileType } from '../models/File'
var Readable = require('stream').Readable
import path from 'path'


class FieldController
{

  constructor(){

  }


  //db.institution.createIndex({name: "text", identityNumber: "text", flats:"text", role:"text"})
  all = async (req, res, next) => {
    try{
      const page = req.query.page ? parseInt(req.query.page) : 1
      const limit = req.query.limit ? parseInt(req.query.limit) : 20
      let skip = page > 1 ? limit*(page-1) : 0
      const textQuery = req.query.textQuery

      let regexQ = new RegExp(textQuery, 'i')
      let findObj = {}
      if(textQuery)
        findObj['$or'] = [{name: regexQ},  {uniqueNumber: regexQ}, {email: regexQ}]

      const institution = await Field.find(findObj, null, {skip:skip, limit:limit}).sort({name: 1}).exec()
      let institutionCount = await Field.countDocuments(findObj)
      let institutionPaginated = new PaginatedModel(institution, institutionCount, skip, limit)
      res.status(200).json(institutionPaginated);
    }catch(e){
      res.status(500).send(e)
    }
   }



  single = async (req, res, next) => {
    try {
      const institutionId = req.params.id
      const institution = await Field.findById(institutionId).exec()
      if (!institution) return next(new Error('Field does not exist'));
      res.status(200).json({
        data: institution
      })
      } catch (error) {
      next(error)
    }
  }

  uploadData = async (req, res, next) => {
    try{
      if(!req.files || !req.files.files || Object.keys(req.files.files).length === 0)
        return res.status(400).send('No files uploaded')

      let currentFile = null

      if(Array.isArray(req.files.files)){
        currentFile = req.files.file[0]
      }else{
        currentFile = req.files.files
      }

      // let mimeType = getFileType(currentFile.mimetype)
      // if(mimeType != 'csv')
      //   return res.status(500).send('Wrong file type')

      var s = new Readable()
      s.push(currentFile.data)
      s.push(null)

      let recordsParser = s.pipe(parse({columns: true}))
      let records = []
      for await (const record of recordsParser) {
        records.push(record)
      }

      let errors = []
      let success = []
      for(let record of records){
        try{

          let mainField = await MainField.findOne({ name: record['Main Field']}).exec()
          if(!mainField){
            mainField = new MainField({ name: record['MainField'] })
            mainField = await mainField.save()
          }
          let outreachCenter = new Field({
            name       : record['Name'] ,
            mainField : mainField._id,
          })
          await outreachCenter.save()
          success.push({ record: record })
        }catch(e){
          errors.push({ record: record, error:e })
        }
      }

      res.status(200).send({ success: success, errors: errors })
    }catch(e){
      res.status(200).send(e)
    }
  }

  add = async(req, res, next) => {
    try{

      const {name , mainField ,} = req.body

      let institution = new Field({
        name      : name ,
        mainField : mainField ,
      })

      await institution.save()
      res.status(200).send({data: institution})
    }
    catch(e){
      next(e)
    }
  }


  update = async(req, res, next) => {
      try{

        const{  name , mainField ,} = req.body
        const id = req.params.id
        let institution = await Field.findOne({_id: id}).exec()

        let properties = Object.keys(req.body)
        properties.forEach((p)=>{
          institution[p] = req.body[p]
        })

        var saved =  await institution.save()
        res.status(200).send({data: saved})
      }
      catch(e){
        next(e)
      }
  }


  downloadExample = async (req, res,) => {
    try{
      let filePath = path.join(__dirname, '../../extra_files/fields.csv')
      res.sendFile(filePath)
    }catch(e){
      throw e
    }
  }




  // delete = async(req, res, next) => {
  //   try{
  //     const id = req.params.id
  //     await Institution.findByIdAndUpdate(id, {status: Deleted})
  //     res.status(200).send()
  //   }catch(e){
  //     res.status(500).send()
  //   }
  // }



}

export default FieldController
