import StatPeriod from '../models/StatPeriod'


class StatPeriodController
{

  constructor(){

  }

  current = async (req, res, next) => {
    try {
      const object = await StatPeriod.findOne().sort({ _id: -1 }).limit(1).exec()
      if (!object) return next(new Error('StatPeriod does not exist'));
      res.status(200).json({
        data: object
      })
      } catch (error) {
      next(error)
    }
  }

  save = async(req, res, next) => {
    try{
      const {start, end} = req.body
      let object = new StatPeriod({start : start, end: end })
      await object.save()
      res.status(200).send({data: object})
    }
    catch(e){
      next(e)
    }
  }

}

export default StatPeriodController
