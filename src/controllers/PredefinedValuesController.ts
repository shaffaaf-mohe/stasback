import * as express from 'express'
import {UserType} from '../models/User'
import {enumToArray, enumToDispValues} from '../utils/utilFuncs'
import islands from '../data/islands'

export default class PredefinedValuesController
{
  constructor() {
  }

  getUserTypes = async function(req:express.Request, res: express.Response, next: express.NextFunction){
    return res.json({data: enumToDispValues(UserType)})
  }

  getAllIslands = async  function(req:express.Request, res: express.Response, next: express.NextFunction){
    //@ts-ignore
    return res.json({data: islands})
  }
  getAtolls = async  function(req:express.Request, res: express.Response, next: express.NextFunction){
    let atolls = Object.keys(islands)
    return res.json({data: atolls})
  }

  getAtollIslands = async  function(req:express.Request, res: express.Response, next: express.NextFunction){
    //@ts-ignore
    return res.json({data: islands[req.params.atoll]})
  }

}

