import mongoose, {Schema} from 'mongoose'
import { v4 as uuidv4 } from 'uuid';
import File, {FileType,IFile, IFileUpload, getFileType} from '../models/File'
import path from 'path'


class FileController
{
  upload = async (req, res, next) => {
    try{
      if(!req.files || !req.files.files || Object.keys(req.files.files).length === 0)
        return res.status(400).send('No files uploaded')
      console.log('files', req.files)

      let savedFiles = []

      if(Array.isArray(req.files.files)){
        for(let file of Object.keys(req.files.files)){
          /* console.log('file', file) */
          let currentFile = req.files.files[file]
          let mimeType = getFileType(currentFile.mimetype)
          let uuid = uuidv4()
          var appDir = path.dirname(require.main.filename)
          console.log('appDir', appDir)
          let filePath = path.join(appDir, `/storage/${uuid}.${mimeType}`)
          console.log('filePath', filePath)
          // let filePath  = `C:/temp/files/${uuid}.${mimeType}`
          // let filePath  = path
          currentFile.mv(filePath, (error)=>{
            if(error)
              throw (error)
          })
          const newFile = new File({fileLocation: filePath, fileType: mimeType })
          await newFile.save()
          savedFiles.push(newFile)
        }
      }else{
        let currentFile = req.files.files
        let mimeType = getFileType(currentFile.mimetype)
        let uuid = uuidv4()
        var appDir = path.dirname(require.main.filename)
        console.log('appDir', appDir)
        let filePath = path.join(appDir, `/storage/${uuid}.${mimeType}`)
        console.log('filePath', filePath)
        currentFile.mv(filePath, (error)=>{
          if(error)
            throw (error)
        })
        const newFile = new File({fileLocation: filePath, fileType: mimeType })
        await newFile.save()
        savedFiles.push(newFile)
      }
      res.status(200).send(savedFiles)
    }catch(e){
      res.status(200).send(e)
    }
  }


  download = async (req, res, next) => {
    let file =  await File.findById(req.params.id).exec()
    res.download(file.fileLocation)
  }

}

export default FileController
