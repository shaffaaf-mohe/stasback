import { text } from 'body-parser';
import * as express from 'express'
import PaginatedModel from '../models/PaginatedModel';
// const User = require('../models/userModel');
import User, {IUser, UserConfirmationMethod,  UserStatus, UserType} from '../models/User'
import RequestAddUser , {IRequestAddUser, RequestUserAddStatuses} from '../models/RequestUserAdd'
import ActionLog, {IActionLog} from '../models/ActionLog'
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
import { getuid, send } from 'process';
import nodeMailer from 'nodemailer'
import { v4 as uuidv4 } from 'uuid';
import MailService from '../services/mailservice'


async function hashPassword(password) {
  return await bcrypt.hash(password, 10);
}

async function validatePassword(plainPassword, hashedPassword) {
  return await bcrypt.compare(plainPassword, hashedPassword);
}

class UserController
{

  constructor(){

  }

  defaultExcludeString: string   = '-password -accessToken -__v'

  allowIfLoggedin = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
    try {
      const user = res.locals.loggedInUser;
      if (!user)
        return res.status(401).json({
          error: "You need to be logged in to access this route"
        });
        // req.user = user;
        next();
    } catch (error) {
      next(error);
    }
  }

  //db.users.createIndex({name: "text", identityNumber: "text", flats:"text", role:"text"})
  all = async (req, res, next) => {
    try{
      const page = req.query.page ? parseInt(req.query.page) : 1
      const limit = req.query.limit ? parseInt(req.query.limit) : 20
      let skip = page > 1 ? limit*(page-1) : 0
      const textQuery = req.query.textQuery
      const status = req.query.status

      let regexQ = new RegExp(textQuery, 'i')
      let findObj = {}
      if(status)
        findObj['status'] = status
      if(textQuery)
        findObj['$or'] = [{name: regexQ}, {type:regexQ}, {identityNumber: regexQ}]

      const users = await User.find(findObj, null, {skip:skip, limit:limit}).select(this.defaultExcludeString).exec()
      let usersCount = await User.countDocuments(findObj)
      let usersPaginated = new PaginatedModel(users, usersCount, skip, limit)
      res.status(200).json(usersPaginated);
    }catch(e){
      res.status(500).send(e)
    }
  }


  allNonPaged = async (req, res, next) => {
    try{
      const textQuery = req.query.textQuery

      let regexQ = new RegExp(textQuery, 'i')
      let findObj = {}
      if(textQuery)
        findObj['$or'] = [{name: regexQ}, {type:regexQ}, {identityNumber: regexQ}]

      const users = await User.find(findObj, null, ).select(this.defaultExcludeString).exec()
      res.status(200).json(users);
    }catch(e){
      res.status(500).send(e)
    }
  }

  single = async (req, res, next) => {
    try {
      const userId = req.params.id
      const user = await User.findById(userId)
      .select(this.defaultExcludeString).exec()
      if (!user) return next(new Error('User does not exist'));
      res.status(200).json({
        data: user
      })
    } catch (error) {
      next(error)
    }
  }

  singleByInvite = async (req, res, next) => {
    try {
      const inviteId = req.params.id
      const user = await User.findOne({ inviteId: inviteId })
      .select(this.defaultExcludeString).exec()
      if (!user) return next(new Error('User does not exist'));
      if(user.status != UserStatus.AutoCreated){
        user.status = UserStatus.AwaitPassword
      }
      res.status(200).json({
        data: user
      })
    } catch (error) {
      next(error)
    }
  }

  newPwdForInvitee = async (req, res, next) => {
    try {
      const { inviteId, password } = req.body
      console.log('req.body', req.body)
      const user = await User.findOne({ inviteId: inviteId }).select(this.defaultExcludeString).exec()
      if (!user) return next(new Error('User does not exist'));
      user.password = await hashPassword(password)
      user.status = UserStatus.Active
      user.inviteId = null
      await user.save()
      res.status(200).json({
        data: user
      })
    } catch (error) {
      next(error)
    }
  }


  update = async(req, res, next) => {
    try{
      const {email, name, type, flats} = req.body
      const id = req.params.id
      let user = await User.findById(id).select(this.defaultExcludeString).exec()
      if(email)
        user.email = email
      if(name)
        user.name = name
      if(type)
        user.type = type

      await user.save()
      res.status(200).send({data: user})
    }
    catch(e){
      next(e)
    }
  }


  activate = async(req, res, next) => {
    try{
      const id = req.params.id
      let user = await User.findById(id).select(this.defaultExcludeString).exec()
      console.log('user', user)
      user.status = UserStatus.Active
      user.userConfirmation = {
        emailConfirmString: '',
        confirmationMethod: UserConfirmationMethod.Management,
        confirmedBy: req.user._id,
      }
      console.log('req_user', req.user)
      await user.save()
      res.status(200).send(user)
    }catch(e){
      res.status(500).json({'e':e})
    }
  }

  deactivate = async(req, res, next) => {
    try{
      const id = req.params.id
      let user = await User.findById(id).select(this.defaultExcludeString).exec()
      console.log('user', user)
      user.status = UserStatus.Inactive
      await user.save()
      res.status(200).send(user)
    }catch(e){
      res.status(500).json({'e':e})
    }
  }

  activateEmail = async(req, res, next) => {
    try{
      const string = req.params.string

      var user  = await User.findOne({'userConfirmation.emailConfirmString': string})
      .select(this.defaultExcludeString)
      .exec()

      if(!user)
        res.status(404).send('Invalid confirmation string')

      user.userConfirmation.confirmationMethod = UserConfirmationMethod.Email
      if(user.status == UserStatus.AwaitPassword){
        user.status = UserStatus.RegisteredAwaitPwd
      }else{
        user.status = UserStatus.Active
      }
      await user.save()
      res.redirect(200, process.env.LOGOUT_URL)
      // res.status(200).send('You have been confirmed, please go back to the application')
    }catch(e){
      res.status(500).send(e)
    }
    res.status(200)
  }

  change = async (req,res,next) =>{
    try{
      const {changes}  = req.body
      console.log('changes', changes)
      const uuid = uuidv4()
      const userId = req.user._id
      let user = await User.findById(userId).exec()
      user.changeConfirmation =  {
        emailConfirmString : uuid,
        changes : changes
      }
      await user.save()
      let emailChange = user.changeConfirmation.changes.find(c => c.changeType == 'email')
      if(emailChange)
        user.email = emailChange.change
      this.sendChangeConfirmationEmail(user)
      res.json({
        data: user,
        message: "An email has been sent to you to confirm the changes. Please check your email"
      })
    }catch(e){
      next(e)
    }
  }

  changeEmail = async(req, res, next) => {
    try{
      const string = req.params.string

      var user  = await User.findOne({'changeConfirmation.emailConfirmString': string})
      .select(this.defaultExcludeString)
      .exec()
      console.log('changes', user)
      if(!user)
        res.status(404).send('Invalid confirmation string')

      for(let change of user.changeConfirmation.changes){
        user[change.changeType] = change.change
      }

      user.changeConfirmation.confirmedBy = user._id

      await user.save()
      res.redirect(200, process.env.LOGOUT_URL)
      // res.status(201).send('You have been confirmed, please go back to the application')
    }catch(e){
      res.status(500).send(e)
    }
    res.status(200)
  }



  changePassword = async(req, res, next) => {
    try{
      const {currentPassword, newPassword} = req.body

      const userId = req.user._id
      console.log( "passwords")
      var user  = await User.findById(userId)
      // .select(this.defaultExcludeString)
      .exec()

      const validPassword = await validatePassword(currentPassword, user.password)
      if (!validPassword) return next(new Error('Password is not correct'))


        user.password = await hashPassword(newPassword)
        await user.save()

        return res.send(200)
    }catch(e){
      res.status(500).send(e)
    }
    res.status(200)
  }

  resetPwd = async(req, res, next) => {
    try{
      const {id} = req.params

      // const userId = req.user._id
      console.log( "passwords")
      var user  = await User.findById(id)
      // .select(this.defaultExcludeString)
      .exec()

      // const validPassword = await validatePassword(currentPassword, user.password)
      // if (!validPassword) return next(new Error('Password is not correct'))


      user.password = await hashPassword('DefaultPassword')
      await user.save()

      return res.send(200)
    }catch(e){
      res.status(500).send(e)
    }
    res.status(200)
  }

  delete = async(req, res, next) => {
    try{
      const id = req.params.id
      await User.findByIdAndUpdate(id, {status: UserStatus.Deleted})
      res.status(200).send()
    }catch(e){
      res.status(500).send()
    }
  }


  ban = async(req, res, next) => {
    try{
      let id = req.params.id
      const user  = await User.findByIdAndUpdate(
        id,
        {status: UserStatus.Inactive},
        {useFindAndModify:true, new:true}
      ).select(this.defaultExcludeString).exec()
      res.status(200).send(user)
    }catch(e){
      res.status(500)
    }
  }


  unban = async(req, res, next) => {
    try{
      let id = req.params.id
      const user  = await User.findByIdAndUpdate(
        id,
        {status: UserStatus.Active},
        {useFindAndModify:true, new:true}
      ).select(this.defaultExcludeString).exec()
      res.status(200).send(user)
    }catch(e){
      res.status(500)
    }
  }


  changeType = async(req,res, next) => {
    try{
      const {id, type} = req.params

      const user = await User.findByIdAndUpdate(
        id,
        {type: type},
        {useFindAndModify: true, new: true}
      ).select(this.defaultExcludeString).exec()

      res.status(200).send(user)
    }catch(e){
      res.status(500)
    }
  }


  changeTenantType = async(req,res, next) => {
    try{
      const {id, type} = req.params

      const user = await User.findByIdAndUpdate(
        id,
        {type: type},
        {useFindAndModify: true, new: true}
      ).select(this.defaultExcludeString).exec()

      res.status(200).send(user)
    }catch(e){
      res.status(500)
    }
  }




  changeFlats = async(req,res, next) => {
    try{
      const {id} = req.params
      const {flats} = req.body

      const user = await User.findByIdAndUpdate(
        id,
        {flats: flats},
        {useFindAndModify: true, new: true}
      ).select(this.defaultExcludeString).exec()

      res.status(200).send(user)
    }catch(e){
      res.status(500)
    }
  }

  sendChangeConfirmationEmail(newUser : IUser){
    try{
      var to = newUser.email
      var subject = 'Raincrest app profile change request'
      var text  =  `Dear ${newUser.name}, Please click the link to verify that you recently requested a change to your profile. Link ${process.env.BASE_URL}/api/user/change/email/${newUser.changeConfirmation.emailConfirmString}`
      var html =  `<p>Dear ${newUser.name}</p><p>Please click the link below to confirm that you recently requested a change to your profile</p><p><a href="${process.env.BASE_URL}/api/users/change/email/${newUser.changeConfirmation.emailConfirmString}"> LINK TO CONFIRMATION </a></p>`

      let mailService = new MailService()
      mailService.sendEmail(to, subject, html, text)
    }catch(e){
      throw e
    }

  }


  requestAddUser = async (req, res, next) => {
    try {
      const currentUserId = req.user._id
      const { identityNumber,  name, email, phoneNumber, viberNumber, type, flats, } = req.body

      const newRequestAddUser = new RequestAddUser({
        user:{
          email: email,
          // identityNumber: identityNumber,
          // name: name,
          phoneNumber:phoneNumber,
          viberNumber: viberNumber,
          type: UserType.HEIStaff,
          status: UserStatus.AwaitPassword,
        },
        requestBy: currentUserId,
        status: RequestUserAddStatuses.Requested,
        actionLog: []
      })
      await newRequestAddUser.save()
      res.json({
        message: "Requested adding of a new user"
      })
    } catch (error) {
      next(error)
    }
  }

  inviteUser = async (req,res,next) => {
    try {

    }catch(e){

    }
  }


  getUserRequests = async (req, res, next) => {
    try{
      const page = req.query.page ? parseInt(req.query.page) : 1
      const limit = req.query.limit ? parseInt(req.query.limit) : 20
      let skip = page > 1 ? limit*(page-1) : 0
      const status = req.query.status
      let statusToSelect = status || RequestUserAddStatuses.Requested
      let findObj = {}

      const requests = await RequestAddUser.find({status:statusToSelect}, null, {skip:skip, limit:limit}).exec()
      let requestsCount = await RequestAddUser.countDocuments({status: statusToSelect})
      let requestsPaginated = new PaginatedModel(requests, requestsCount, skip, limit)
      res.status(200).json(requestsPaginated);
    }catch(e){
      return res.status(500).send(e)
    }
  }

  getUserRequest = async (req, res, next) => {
    try{
      const id  = req.params.id
      const request = await RequestAddUser.findById(id)
      res.status(200).json(request);
    }catch(e){
      return res.status(500).send(e)
    }
  }

  agreeRequestAddUser = async (req, res, next) =>{
    try{
      const {requestId} = req.params

      let requestAddUser = await RequestAddUser.findById(requestId).exec()
      let password = this.makeid(9)
      let user  = new User({
        name: requestAddUser.user.name,
        // identityNumber: requestAddUser.user.identityNumber,
        email: requestAddUser.user.email,
        type: requestAddUser.user.type,
        status: UserStatus.Registered,
        password: await hashPassword(password),
        userConfirmation: {emailConfirmString:uuidv4()}
      })

      await user.save()
      requestAddUser.status = RequestUserAddStatuses.Approved
      requestAddUser.addedUserId = user._id
      await requestAddUser.save()
      this.sendAddUserEmail(user , password)
      return res.status(200).send('User request has been accepted')
    }catch(e){
      return res.status(500).send(e)
    }
  }


  rejectRequestAddUser = async (req, res, next) =>{
    try{
      const {requestId} = req.params
      let requestAddUser = await RequestAddUser.findById(requestId).exec()
      requestAddUser.status = RequestUserAddStatuses.Rejected
      await requestAddUser.save()
      return res.status(200).send('User request has been rejected')
    }catch(e){
      return res.status(500).send(e)
    }
  }

  resendAddUserEmail = async (req,res,next ) => {
    try{
      const {requestId}  = req.params
      let requestAddUser = await RequestAddUser.findById(requestId).exec()
      let user  = await User.findById(requestAddUser.addedUserId).exec()
      let password = this.makeid(9)
      let hashedPassword = await hashPassword(password)
      user.password = hashedPassword
      this.sendAddUserEmail(user, password)
      return res.status(200).send('New email sent with new password')
    }catch(e){
      return res.status(500).send(e)
    }
  }

  sendAddUserEmail(newUser : IUser, password:string){
    try{
      let to = newUser.email
      let subject =  "Please confirm your registration to the raincrest app"
      let html =  `<p>Dear ${newUser.name}</p><p>Please click the link below to confirm your Raincrest app account</p><p><a href="${process.env.BASE_URL}/api/users/activate/email/${newUser.userConfirmation.emailConfirmString}"> LINK TO CONFIRMATION </a></p> <p>Your temporary password is ${password}. Please change after login by vising your profile menu.</p>`
      let text =  `Dear ${newUser.name}, Please click the link to verify your account. Link ${process.env.BASE_URL}/api/user/activate/email/${newUser.userConfirmation.emailConfirmString} . Your temporary password is ${password}. Please change after login, by visting your profile menu.`

      let mailer = new MailService()
      mailer.sendEmail(to, subject, html, text)
    }catch(e){

    }

  }

  sendInviteLinkA = async(req,res, next) => {
    try{
      const id = req.params.id
      let user = await User.findOne({ _id: id })
      if(!user)
        return res.status(404).send('User not found')

      this.sendInviteLink(user)
      res.status(200).send('Link has been sent.')
    }
    catch(e){
      next(e)
    }
  }


  async sendInviteLink(newUser : IUser){
    try{
      const uuid = uuidv4()
      newUser.inviteId = uuid
      newUser.status = UserStatus.Invited
      await newUser.save()
      var to = newUser.email
      var subject = 'Invite to use Higher Education Statistics Gathering System'
      var text  =  `Dear ${newUser.name}, Please click the link to change your password. Link ${process.env.BASE_URL}/api/user/change/email/${newUser.inviteId}`
      var html =  `<p>Dear ${newUser.name}</p><p>Please click the link click the link below to change your password</p><p><a href="${process.env.BASE_URL}/setpwd/${newUser.inviteId}"> LINK TO CONFIRMATION </a></p>`

      let mailService = new MailService()
      console.log('html',html )
      mailService.sendEmail(to, subject, html, text)
    }catch(e){
      throw e
    }
  }

  makeid = (length) => {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
}

export default UserController
