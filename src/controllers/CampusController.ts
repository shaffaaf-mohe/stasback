import PaginatedModel from '../models/PaginatedModel'
import Campus from '../models/Campus'
import Institution from '../models/Institution'
import Person from '../models/Person'
import {parse} from 'csv-parse'
import { getFileType } from '../models/File'
import User, { IUser } from '../models/User'
import { UserStatus, UserType } from '../models/User'
import nodeMailer from 'nodemailer'
import { v4 as uuidv4 } from 'uuid';
import MailService from '../services/mailservice'
import path from 'path'
var Readable = require('stream').Readable


class CampusController
{

  constructor(){

  }


  //db.institution.createIndex({name: "text", identityNumber: "text", flats:"text", role:"text"})
  all = async (req, res, next) => {
    try{
      const page = req.query.page ? parseInt(req.query.page) : 1
      const limit = req.query.limit ? parseInt(req.query.limit) : 20
      let skip = page > 1 ? limit*(page-1) : 0
      const textQuery = req.query.textQuery

      let regexQ = new RegExp(textQuery, 'i')
      let findObj = {}
      if(textQuery)
        findObj['$or'] = [{name: regexQ},  {uniqueNumber: regexQ}, {email: regexQ}]

      const institution = await Campus.find(findObj, null, {skip:skip, limit:limit}).sort({name: 1}).exec()
      let institutionCount = await Campus.countDocuments(findObj)
      let institutionPaginated = new PaginatedModel(institution, institutionCount, skip, limit)
      res.status(200).json(institutionPaginated);
    }catch(e){
      res.status(500).send(e)
    }
   }



  single = async (req, res, next) => {
    try {
      const institutionId = req.params.id
      const institution = await Campus.findById(institutionId).exec()
      if (!institution) return next(new Error('Campus does not exist'));
      res.status(200).json({
        data: institution
      })
      } catch (error) {
      next(error)
    }
  }


  uploadData = async (req, res, next) => {
    try{
      if(!req.files || !req.files.files || Object.keys(req.files.files).length === 0)
        return res.status(400).send('No files uploaded')

      let currentFile = null

      if(Array.isArray(req.files.files)){
        currentFile = req.files.file[0]
      }else{
        currentFile = req.files.files
      }

      // let mimeType = getFileType(currentFile.mimetype)
      // if(mimeType != 'csv')
      //   return res.status(500).send('Wrong file type')

      var s = new Readable()
      s.push(currentFile.data)
      s.push(null)

      let recordsParser = s.pipe(parse({columns: true}))
      let records = []
      for await (const record of recordsParser) {
        records.push(record)
      }

      let errors = []
      let success = []
      for(let record of records){
        try{
          let person = await Person.findOne({ nid: record['Primary Contact Id']}).exec()
          if(!person){
            person = new Person({ name: record['Primary Contact Name'], nid: record['Primary Contact Id'], email: record['Primary Contact Id' ] })
            await person.save()
          }
          else{
            if(person.name != record['Primary Contact Name'])
              throw('Primary Contact does not match one already in database version:' + person)
          }

          let institution = await Institution.findOne({ name: record['Owner Name']}).exec()
          if(!institution){
            throw('Owner is not added yet. Please add owner first.')
          }

          let campus = new Campus({
            name               : record['Name'] ,
            isGovt             : record['Govt']  == 'TRUE' ? true    : false ,
            isPrimary          : record['Primary']  == 'TRUE' ? true : false ,
            registrationNumber : record['Registration Number'] ,
            email              : record['Email'] ,
            address            : record['Address'] ,
            ownerId            : institution._id,
            primaryContact     : person._id ,
            atoll              : record['Address'] ,
            island             : record['Island']}
          )
          await campus.save()
          success.push({ record: record })
        }catch(e){
          errors.push({ record: record, error:e })
        }
      }

      res.status(200).send({ success: success, errors: errors })
    }catch(e){
      res.status(200).send(e)
    }
  }

  add = async(req, res, next) => {
    try{

      const {name , isGovt , isPrimary , registrationNumber , email , address , atoll , island , primaryContact , ownerId ,} = req.body
      let institution = new Campus({name : name , isGovt : isGovt , isPrimary : isPrimary , registrationNumber : registrationNumber , email : email , address : address , atoll : atoll , island : island , primaryContact : primaryContact , ownerId : ownerId ,})
      await institution.save()
      res.status(200).send({data: institution})
    }
    catch(e){
      next(e)
    }
  }


  update = async(req, res, next) => {
      try{

        const{  name , isGovt , registrationNumber , email , address , primaryContact ,} = req.body
        const id = req.params.id
        let institution = await Campus.findOne({_id: id}).exec()

        let properties = Object.keys(req.body)
        properties.forEach((p)=>{
          institution[p] = req.body[p]
        })

        var saved =  await institution.save()
        let user = await User.findOne({ relatedHEI: id })
        user.email = institution.email
        user.save()
        res.status(200).send({data: saved})
      }
      catch(e){
        next(e)
      }
  }



  sendInviteLinkA = async(req,res, next) => {
    try{

      const id = req.params.id
      let user = await User.findOne({ relatedHEI: id })
      if(!user){
        user = await this.createUserForInstitution(id)
      }
      else {
        user.status = UserStatus.Invited
        await user.save()
      }

      this.sendInviteLink(user)
      res.status(200).send('Link has been sent.')
    }
    catch(e){
      next(e)
    }
  }

  async sendInviteLink(newUser : IUser){
    try{
      const uuid = uuidv4()
      newUser.inviteId = uuid
      await newUser.save()
      var to = newUser.email
      var subject = 'Invite to use Higher Education Statistics Gathering System'
      var text  =  `Dear ${newUser.name}, Please click the link to verify that you recently requested a change to your profile. Link ${process.env.BASE_URL}/api/user/change/email/${newUser.inviteId}`
      var html =  `<p>Dear ${newUser.name}</p><p>Please click the link below to confirm that you recently requested a change to your profile</p><p><a href="${process.env.BASE_URL}/setpwd/${newUser.inviteId}"> LINK TO CONFIRMATION </a></p>`

      let mailService = new MailService()
      console.log('html',html )
      mailService.sendEmail(to, subject, html, text)
    }catch(e){
      throw e
    }
  }



  createUserForInstitution = async (institutionId) => {
    try{

      let institution = await Campus.findById(institutionId)
      let parent = await Institution.findById(institution.ownerId)
      let associatedUser = new User({
        name       : `${institution.name}-${parent.name}`,
        email      : institution.email,
        type       : UserType.HEIStaff,
        status     : UserStatus.AutoCreated,
        relatedHEI : institution._id,
        heiType    : 'Campus',
      })
      await associatedUser.save()
      return associatedUser
    }catch(e){
      throw e
    }
  }


  downloadExample = async (req, res,) => {
    try{
      let filePath = path.join(__dirname, '../../extra_files/campuses.csv')
      res.sendFile(filePath)
    }catch(e){
      throw e
    }
  }


  // delete = async(req, res, next) => {
  //   try{
  //     const id = req.params.id
  //     await Institution.findByIdAndUpdate(id, {status: Deleted})
  //     res.status(200).send()
  //   }catch(e){
  //     res.status(500).send()
  //   }
  // }



}

export default CampusController
