import moment from 'moment'
import nodeMailer from 'nodemailer'
import { v4 as uuidv4 } from 'uuid';
import path from 'path'
import pdftk from 'node-pdftk'

class TestController
{
  sortedFilesLocation:string
  unsortedFilesLocation:string

  constructor(){
   this.sortedFilesLocation = process.env.SORTED_FILES_LOCATION
   this.unsortedFilesLocation = process.env.UNSORTED_FILES_LOCATION
  }

  test = async(req,res,next) => {
    try{
      console.log('Inside DocumentsController, unsorted', this.unsortedFilesLocation)
      const fs = require('fs');

      fs.readdir(this.unsortedFilesLocation, {withFileTypes:true,}, (err, files) => {
        let filesRead = []
        files.forEach(file => {
          if(file['isFile']())
          {
            const filePath = this.unsortedFilesLocation + file.name
            const extension = path.extname(filePath)
            const fileInfo = fs.statSync(filePath) || null
            const fileSize = fileInfo.size
            if(extension == '.pdf')
              filesRead.push({name:file.name, type:extension, size:fileSize, created: fileInfo.mtime})
          }

        });
        res.status(200).send(filesRead)
      });
    }catch(e){

    }
  }

  record = async (req, res, next) => {
    try{
      var { start, end, name } = req.body
      const oldFilePath = this.unsortedFilesLocation + name
      const newName =  'i'+name
      const newFilePath = this.unsortedFilesLocation + newName

      let pdfInformationBuffer = await pdftk.input({A:oldFilePath}).dumpData().output()
      let inputFileInformation = pdfInformationBuffer.toString('utf-8')
      let originalFileNumberOfPages = parseInt(inputFileInformation.match(/NumberOfPages:(.*)/)[1])

      await pdftk.input({A:oldFilePath}).cat(`A${start}-${end}`).output(newFilePath)

      if(start != 1 && end != originalFileNumberOfPages){
        await pdftk.input({A:oldFilePath}).cat(`A${1}-${start-1} A${end+1}-end`).output(oldFilePath)
      }else if(end != originalFileNumberOfPages){
        await pdftk.input({A:oldFilePath}).cat(`A${end+1}-end`).output(oldFilePath)
      }

      console.log('pdfInformation', inputFileInformation)
      res.status(200).send('Success')

    }catch(e){
      throw e
    }
  }

}



export default TestController
