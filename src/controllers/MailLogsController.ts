import PaginatedModel from '../models/PaginatedModel'
import MailLog from '../models/MailLog'


class MailLogController
{

  constructor(){

  }


  //db.object.createIndex({name: "text", identityNumber: "text", flats:"text", role:"text"})
  all = async (req, res, next) => {
    try{
      const page = req.query.page ? parseInt(req.query.page) : 1
      const limit = req.query.limit ? parseInt(req.query.limit) : 20
      let skip = page > 1 ? limit*(page-1) : 0
      const textQuery = req.query.textQuery

      let regexQ = new RegExp(textQuery, 'i')
      let findObj = {}
      if(textQuery)
        findObj['$or'] = [{name: regexQ},  {uniqueNumber: regexQ}, {email: regexQ}]

      const object = await MailLog.find(findObj, null, {skip:skip, limit:limit}).exec()
      let objectCount = await MailLog.countDocuments(findObj)
      let objectPaginated = new PaginatedModel(object, objectCount, skip, limit)
      res.status(200).json(objectPaginated);
    }catch(e){
      res.status(500).send(e)
    }
   }



}

export default MailLogController
