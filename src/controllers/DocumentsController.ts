import Document, {IDocument} from '../models/DocumentRecord'
import Person from '../models/Person'
const jwt = require('jsonwebtoken')
import PaginatedModel from '../models/PaginatedModel'
import { v4 as uuidv4 } from 'uuid'
import path from 'path'
import fs from 'fs'
import mongoose from 'mongoose'
import pdftk from 'node-pdftk'
import ActionLog from '../models/ActionLog'
import Entity from '../models/Entity'

class DocumentsController
{
  sortedFilesLocation:string
  unsortedFilesLocation:string

  constructor(){
   this.sortedFilesLocation = process.env.SORTED_FILES_LOCATION
   this.unsortedFilesLocation = process.env.UNSORTED_FILES_LOCATION
   console.log('process env ', process.env.SORTED_FILES_LOCATION)
  }

  sorted = async (req, res, next) => {
    try{
      const page = req.query.page ? parseInt(req.query.page) : 1
      const limit = req.query.limit ? parseInt(req.query.limit) : 20
      let skip = page > 1 ? limit*(page-1) : 0
      const textQuery = req.query.textQuery
      const relatedParty = req.query.relatedParty

      let regexQ = new RegExp(textQuery, 'i')
      let findObj = {}
      if(textQuery){
        findObj['$or'] =
        [
          {documentType: regexQ},
          {fileNumber:regexQ},
          {'caseNumbers.ref': {$in: regexQ}},
          {'caseNumbers.type': {$in: regexQ}},
          {references: {$in: regexQ}},
        ]
        // findObj['$text']  = {'$search': textQuery}
      }
      if(relatedParty){
        findObj['relatedParties'] = {$in: relatedParty}
      }

      const document = await Document.find(findObj, null, {skip:skip, limit:limit}).sort({_id: 1}).exec()
      let documentCount = await Document.countDocuments(findObj)
      let documentPaginated = new PaginatedModel(document, documentCount, skip, limit)
      res.status(200).json(documentPaginated);
    }catch(e){
      res.status(500).send(e)
    }
  }


  verified = async (req, res, next) => {
    try{
      const page = req.query.page ? parseInt(req.query.page) : 1
      const limit = req.query.limit ? parseInt(req.query.limit) : 20
      let skip = page > 1 ? limit*(page-1) : 0
      const textQuery = req.query.textQuery
      const relatedParty = req.query.relatedParty

      let regexQ = new RegExp(textQuery, 'i')
      let findObj = {}
      if(textQuery){
        findObj['$or'] =
        [
          {documentType: regexQ},
          {fileNumber:regexQ},
          {'caseNumbers.ref': {$in: regexQ}},
          {'caseNumbers.type': {$in: regexQ}},
          {references: {$in: regexQ}},
        ]
        // findObj['$text']  = {'$search': textQuery}
      }
      if(relatedParty){
        findObj['relatedParties'] = {$in: relatedParty}
      }

      findObj['verified'] = true

      const document = await Document.find(findObj, null, {skip:skip, limit:limit}).sort({_id: 1}).exec()
      let documentCount = await Document.countDocuments(findObj)
      let documentPaginated = new PaginatedModel(document, documentCount, skip, limit)
      res.status(200).json(documentPaginated);
    }catch(e){
      res.status(500).send(e)
    }
  }

  rejected = async (req, res, next) => {
    try{
      const page = req.query.page ? parseInt(req.query.page) : 1
      const limit = req.query.limit ? parseInt(req.query.limit) : 20
      let skip = page > 1 ? limit*(page-1) : 0
      const textQuery = req.query.textQuery
      const relatedParty = req.query.relatedParty

      let regexQ = new RegExp(textQuery, 'i')
      let findObj = {}
      if(textQuery){
        findObj['$or'] =
        [
          {documentType: regexQ},
          {fileNumber:regexQ},
          {'caseNumbers.ref': {$in: regexQ}},
          {'caseNumbers.type': {$in: regexQ}},
          {references: {$in: regexQ}},
        ]
        // findObj['$text']  = {'$search': textQuery}
      }
      if(relatedParty){
        findObj['relatedParties'] = {$in: relatedParty}
      }
      findObj['rejected'] = true

      const document = await Document.find(findObj, null, {skip:skip, limit:limit}).sort({_id: 1}).exec()
      let documentCount = await Document.countDocuments(findObj)
      let documentPaginated = new PaginatedModel(document, documentCount, skip, limit)
      res.status(200).json(documentPaginated);
    }catch(e){
      res.status(500).send(e)
    }
  }

  unverified = async (req, res, next) => {
    try{
      const page = req.query.page ? parseInt(req.query.page) : 1
      const limit = req.query.limit ? parseInt(req.query.limit) : 20
      let skip = page > 1 ? limit*(page-1) : 0
      const textQuery = req.query.textQuery
      const relatedParty = req.query.relatedParty
      const fileNumber = req.query.fileNumber

      let regexQ = new RegExp(textQuery, 'i')
      let findObj = {}
      if(textQuery){
        findObj['$or'] =
        [
          {documentType: regexQ},
          {fileNumber:regexQ},
          {'caseNumbers.ref': {$in: regexQ}},
          {'caseNumbers.type': {$in: regexQ}},
          {references: {$in: regexQ}},
        ]
        // findObj['$text']  = {'$search': textQuery}
      }
      if(relatedParty){
        findObj['relatedParties'] = {$in: relatedParty}
      }

      if(fileNumber){
        findObj['fileNumber'] = fileNumber
      }
      findObj['verified'] = false
      findObj['rejected'] = false

      const document = await Document.find(findObj, null, {skip:skip, limit:limit}).sort({_id: 1}).exec()
      let documentCount = await Document.countDocuments(findObj)
      let documentPaginated = new PaginatedModel(document, documentCount, skip, limit)
      res.status(200).json(documentPaginated);
    }catch(e){
      res.status(500).send(e)
    }
  }

  filesInUnverified = async (req,res,next) => {
    try {
      Document.distinct('fileNumber', {verified: false}, function (err, result) {
        if (err) throw err
        res.status(200).send(result)
      })
    }catch(e){
      res.status(500).send(e)
    }
  }

  unsorted = async (req, res, next) => {
    try{
      console.log('Inside DocumentsController, unsorted', this.unsortedFilesLocation)
      const fs = require('fs');

      fs.readdir(this.unsortedFilesLocation, {withFileTypes:true,}, (err, files) => {
        let filesRead = []
        files.forEach(file => {
          if(file['isFile']())
          {
            const filePath = this.unsortedFilesLocation + file.name
            const extension = path.extname(filePath)
            const fileInfo = fs.statSync(filePath) || null
            const fileSize = fileInfo.size
            if(extension == '.pdf')
              filesRead.push({name:file.name, type:extension, size:fileSize, created: fileInfo.mtime})
          }

        });
        res.status(200).send(filesRead)
      });
    }catch(e){
      res.status(500).send({ message: e })
      throw e
    }
  }


  downloadUnsorted = async (req, res, next) => {
    try{
        var file = fs.createReadStream(this.unsortedFilesLocation + req.params.fileName)
        var stat = fs.statSync(this.unsortedFilesLocation + req.params.fileName)
        res.setHeader('Content-Length', stat.size)
        res.setHeader('Content-Type', 'application/pdf')
        res.setHeader('Content-Disposition', 'attachment filename=quote.pdf')
        file.pipe(res)
    }catch(e){
      res.status(500).send({ message: e })
      throw e
    }
  }

  deleteUnsorted = async (req, res, next) => {
    try{
      fs.unlinkSync(this.unsortedFilesLocation + req.params.fileName)
      res.status(200).send({message: 'success'})
    }catch(e){
      res.status(500).send({ message: e })
      throw e
    }
  }

  downloadSorted = async (req, res, next) => {
    try{
      var record =  await Document.findById(req.params.id)
      var file = fs.createReadStream(this.sortedFilesLocation + record.newFileName)
      var stat = fs.statSync(this.sortedFilesLocation + record.newFileName)
      res.setHeader('Content-Length', stat.size)
      res.setHeader('Content-Type', 'application/pdf')
      res.setHeader('Content-Disposition', 'attachment filename=quote.pdf')
      file.pipe(res)
    }catch(e){
      res.status(500).send({ message: e })
      throw e
    }
  }

  record = async (req, res, next) => {
    try{
      var document = new Document(req.body)
      var ministry = await Entity.findOne({refNumber: 'MoHE'}).exec()
      if(!document.relatedParties.find(rp => ministry._id)){
        document.relatedParties.push(ministry._id)
      }
      let newFileName = uuidv4()

      var oldPath = this.unsortedFilesLocation + document.oldFileName
      var newPath = this.sortedFilesLocation + newFileName
      let action = new ActionLog({actionBy:req.user._id, actionTaken: 'Added New'})


      let pdfInformationBuffer = await pdftk.input({A:oldPath}).dumpData().output()
      let inputFileInformation = pdfInformationBuffer.toString('utf-8')
      let originalFileNumberOfPages = parseInt(inputFileInformation.match(/NumberOfPages:(.*)/)[1])

      let start = document.originalStart
      let end = document.originalEnd

      await pdftk.input({A:oldPath}).cat(`A${start}-${end}`).output(newPath)

      if(start != 1 && end != originalFileNumberOfPages){
        await pdftk.input({A:oldPath}).cat(`A${1}-${start-1} A${end+1}-end`).output(oldPath)
      }else if(end != originalFileNumberOfPages){
        await pdftk.input({A:oldPath}).cat(`A${end+1}-end`).output(oldPath)
      }
      else{
        fs.unlinkSync(oldPath)
      }

      document.newFileName = newFileName
      document.editHistory = [action]
      document.save()

      if(end != originalFileNumberOfPages)
        document.notLast = true
      res.status(200).send(document)

    }catch(e){
      res.status(500).send({ message: e })
      throw e
    }
  }

  updateRecord = async (req, res, next) => {
    try{
      const {fileNumber, documentType, documentDate, references, caseNumbers, relatedParties} = req.body
      var document =  await Document.findById(req.params.id)
      if(document.verified)
        return res.status(403).send({message: 'Document already verified'})
      var action = new ActionLog({actionBy: req.user._id, actionTaken: 'Edited'})

      if(fileNumber)
        document.fileNumber = fileNumber
      if(documentType)
        document.documentType = documentType
      if(documentDate)
        document.documentDate = documentDate
      if(references)
        document.references = references
      if(caseNumbers)
        document.caseNumbers = caseNumbers
      if(relatedParties)
        document.relatedParties = relatedParties

      document.rejected = false
      document.editHistory.push(action)

      await document.save()
      res.status(200).send({data: document})

    }catch(e){
      res.status(500).send({ message: e })
      throw e
    }
  }

  getSingle = async (req, res, next) => {
    try{
      var record =  await Document.findById(req.params.id)
      res.status(200).send(record)
    }catch(e){
      res.status(500).send({ message: e })
      throw e
    }
  }

  distinctNames = async (req, res, next) => {
    try{
      console.log('inside names', )
      Document.distinct('documentType', function (err, result) {
        if (err) throw err
        res.status(200).send(result.filter(r => r != null))
      })
    }catch(e){
      res.status(500).send({ message: e })
      throw e
    }
  }


  distinctCaseTypes = async (req, res, next) => {
    try{
      Document.distinct('caseNumbers.type', function (err, result) {
        if (err) throw err
        res.status(200).send(result.filter(r => r != null))
      })
    }catch(e){
      res.status(500).send({ message: e })
      throw e
    }
  }

  verify = async (req, res, next) => {
    try{
      var document =  await Document.findById(req.params.id)
      var action = new ActionLog({actionBy: req.user._id, actionTaken: 'Verified'})
      document.verified = true
      document.rejected = false
      document.editHistory.push(action)
      await document.save()
      res.status(200).send({data: document})
    }catch(e){
      res.status(500).send({ message: e })
      throw e
    }
  }

  reject = async (req, res, next) => {
    try{
      var reason = req.body.rejectReason
      if(!reason)
        throw 'should give reason'
      var document =  await Document.findById(req.params.id)
      var action = new ActionLog({actionBy: req.user._id, actionTaken: 'Rejected'})
      document.verified = false
      document.rejected = true
      document.rejectReason = reason
      document.editHistory.push(action)
      await document.save()
      res.status(200).send({data: document})
    }catch(e){
      res.status(500).send({ message: e })
      throw e
    }
  }

}

export default DocumentsController
