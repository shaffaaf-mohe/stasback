import PaginatedModel from '../models/PaginatedModel';
import Entity from '../models/Entity'
import ActionLog from '../models/ActionLog'



class EntityController
{

  constructor(){

  }


  //db.entity.createIndex({name: "text", refNumber: "text", flats:"text", role:"text"})
  all = async (req, res, next) => {
    try{
      const page = req.query.page ? parseInt(req.query.page) : 1
      const limit = req.query.limit ? parseInt(req.query.limit) : 20
      let skip = page > 1 ? limit*(page-1) : 0
      const textQuery = req.query.textQuery

      let regexQ = new RegExp(textQuery, 'i')
      let findObj = {}
      if(textQuery)
        findObj['$or'] = [{name: regexQ}, {type:regexQ}, {refNumber: regexQ}, {caseNumbers: {$in: regexQ}}]

      const entity = await Entity.find(findObj, null, {skip:skip, limit:limit}).exec()
      let entityCount = await Entity.countDocuments(findObj)
      let entityPaginated = new PaginatedModel(entity, entityCount, skip, limit)
      res.status(200).json(entityPaginated);
    }catch(e){
      res.status(500).send(e)
    }
   }



  single = async (req, res, next) => {
    try {
      const entityId = req.params.id
      const entity = await Entity.findById(entityId).exec()
      if (!entity) return next(new Error('Entity does not exist'));
      res.status(200).json({
        data: entity
      })
      } catch (error) {
      next(error)
    }
  }


  add = async(req, res, next) => {
    try{
      const {name, refNumber, email, address, caseNumbers, isGovt, isInstitution} = req.body
      let entity = new Entity({email:email, refNumber:refNumber, name:name, address:address, caseNumbers: caseNumbers, isGovt:isGovt, isInstitution:isInstitution})
      await entity.save()
      res.status(200).send({data: entity})
    }
    catch(e){
      next(e)
    }
  }


  update = async(req, res, next) => {
      try{
        const {name, refNumber, email, address, caseNumbers, isGovt, isInstitution} = req.body
        const id = req.params.id
        let entity = await Entity.findById(id).exec()

        if(email)
          entity.email = email
        if(name)
          entity.name = name
        if(address)
          entity.address = address
        if(isGovt)
          entity.isGovt = isGovt
        if(isInstitution)
          entity.isInstitution = isInstitution
        if(refNumber)
          entity.refNumber = refNumber
        if(caseNumbers)
          entity.caseNumbers = caseNumbers

      var action = new ActionLog({actionBy: req.user._id, actionTaken: 'Edited'})

      // const requestKeys = Object.keys(req.body)
      // for (let i = 0; i < requestKeys.length; i++) {
      //   if(entity[requestKeys[i]])
      //     entity[requestKeys[i]] = req.params[requestKeys[i]]
      // }
      // entity.editHistory.push(action)

      await entity.save()
      res.status(200).send({data: entity})
        await entity.save()
        res.status(200).send({data: entity})
      }
      catch(e){
        next(e)
      }
  }






  // delete = async(req, res, next) => {
  //   try{
  //     const id = req.params.id
  //     await Entity.findByIdAndUpdate(id, {status: Deleted})
  //     res.status(200).send()
  //   }catch(e){
  //     res.status(500).send()
  //   }
  // }



}

export default EntityController
