import PaginatedModel from '../models/PaginatedModel';
import Person from '../models/Person'
import path from 'path'
import {parse} from 'csv-parse'
import { getFileType } from '../models/File'
var Readable = require('stream').Readable


class PersonController
{

  constructor(){

  }


  //db.person.createIndex({name: "text", identityNumber: "text", flats:"text", role:"text"})
  all = async (req, res, next) => {
    try{
      const page = req.query.page ? parseInt(req.query.page) : 1
      const limit = req.query.limit ? parseInt(req.query.limit) : 20
      let skip = page > 1 ? limit*(page-1) : 0
      const textQuery = req.query.textQuery

      let regexQ = new RegExp(textQuery, 'i')
      let findObj = {}
      if(textQuery)
        findObj['$or'] = [{name: regexQ}, {'identityNumbers.id': regexQ}, { 'phoneNumbers': regexQ }]

      const person = await Person.find(findObj, null, {skip:skip, limit:limit}).exec()
      let personCount = await Person.countDocuments(findObj)
      let personPaginated = new PaginatedModel(person, personCount, skip, limit)
      res.status(200).json(personPaginated);
    }catch(e){
      res.status(500).send(e)
    }
   }



  single = async (req, res, next) => {
    try {
      const personId = req.params.id
      const person = await Person.findById(personId).exec()
      if (!person) return next(new Error('Person does not exist'));
      res.status(200).json({
        data: person
      })
      } catch (error) {
      next(error)
    }
  }


  add = async(req, res, next) => {
    try{
      const {  name , nid, passport, email , address, country, phoneNumber, phoneNumber2, } = req.body

      let person = new Person({
        name: name ,
        nid: nid,
        passport: passport,
        email: email ,
        address: address,
        phoneNumber: phoneNumber
      })

      await person.save()
      res.status(200).send({data: person})
    }
    catch(e){
      next(e)
    }
  }


  update = async(req, res, next) => {
      try{
      const {  name , ids  , email , address, mobileNumbers } = req.body
        const id = req.params.id
        let person = await Person.findById(id).exec()

        let properties = Object.keys(req.body)
        properties.forEach((p)=>{
          person[p] = req.body[p]
        })

        await person.save()
        res.status(200).send({data: person})
      }
      catch(e){
        next(e)
      }
  }

  uploadData = async (req, res, next) => {
    try{
      if(!req.files || !req.files.files || Object.keys(req.files.files).length === 0)
        return res.status(400).send('No files uploaded')

      let currentFile = null

      if(Array.isArray(req.files.files)){
        currentFile = req.files.file[0]
      }else{
        currentFile = req.files.files
      }

      // let mimeType = getFileType(currentFile.mimetype)
      // if(mimeType != 'csv')
      //   return res.status(500).send('Wrong file type')

      var s = new Readable()
      s.push(currentFile.data)
      s.push(null)

      let recordsParser = s.pipe(parse({columns: true}))
      let records = []
      for await (const record of recordsParser) {
        records.push(record)
      }

      let errors = []
      let success = []
      for(let record of records){
        try{

          let person = new Person({
            name  : record['Name'] ,
            nid: record['NID'],
            passport: record['PP'],
            email: record['Email'],
            address: record['Address'],
            country: record['Country'],
            phoneNumber: record['Phone 1'],
            phoneNumber2: record['Phone 2'],
          })
          await person.save()
          success.push({ record: record })
        }catch(e){
          errors.push({ record: record, error:e })
        }
      }

      res.status(200).send({ success: success, errors: errors })
    }catch(e){
      res.status(200).send(e)
    }
  }


  downloadExample = async (req, res,) => {
    try{
      let filePath = path.join(__dirname, '../../extra_files/people.csv')
      res.sendFile(filePath)
    }catch(e){
      throw e
    }
  }




  // delete = async(req, res, next) => {
  //   try{
  //     const id = req.params.id
  //     await Person.findByIdAndUpdate(id, {status: Deleted})
  //     res.status(200).send()
  //   }catch(e){
  //     res.status(500).send()
  //   }
  // }



}

export default PersonController
