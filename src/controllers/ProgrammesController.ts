import PaginatedModel from '../models/PaginatedModel'
import Programme from '../models/Programme'
import {parse} from 'csv-parse'
//@ts-ignore
import { getFileType } from '../models/File'
import Field from '../models/Field'
import path from 'path'
var Readable = require('stream').Readable


class ProgrammeController
{

  constructor(){

  }


  //db.programme.createIndex({name: "text", identityNumber: "text", flats:"text", role:"text"})
  all = async (req, res, next) => {
    try{
      const page = req.query.page ? parseInt(req.query.page) : 1
      const limit = req.query.limit ? parseInt(req.query.limit) : 20
      let skip = page > 1 ? limit*(page-1) : 0
      const textQuery = req.query.textQuery

      let regexQ = new RegExp(textQuery, 'i')
      let findObj = {}
      if(textQuery)
        findObj['$or'] = [{name: regexQ},  {uniqueNumber: regexQ}, {email: regexQ}]

      const programme = await Programme.find(findObj, null, {skip:skip, limit:limit}).sort({ level: -1, name: 1 }).exec()
      let programmeCount = await Programme.countDocuments(findObj)
      let programmePaginated = new PaginatedModel(programme, programmeCount, skip, limit)
      res.status(200).json(programmePaginated);
    }catch(e){
      res.status(500).send(e)
    }
  }

  allNonPaged = async (req, res, next) => {
    try{
      const programmes = await Programme.find({}).exec()
      res.status(200).send(programmes)
    }catch(e){
      throw e
    }
  }

  single = async (req, res, next) => {
    try {
      const programmeId = req.params.id
      const programme = await Programme.findById(programmeId).exec()
      if (!programme) return next(new Error('Programme does not exist'));
      res.status(200).json({
        data: programme
      })
      } catch (error) {
      next(error)
    }
  }

  uploadData = async (req, res, next) => {
    try{
      if(!req.files || !req.files.files || Object.keys(req.files.files).length === 0)
        return res.status(400).send('No files uploaded')

      let currentFile = null

      if(Array.isArray(req.files.files)){
        currentFile = req.files.file[0]
      }else{
        currentFile = req.files.files
      }

      // let mimeType = getFileType(currentFile.mimetype)
      // if(mimeType != 'csv')
      //   return res.status(500).send('Wrong file type')

      var s = new Readable()
      s.push(currentFile.data)
      s.push(null)

      let recordsParser = s.pipe(parse({columns: true}))
      let records = []
      for await (const record of recordsParser) {
        records.push(record)
      }

      let errors = []
      let success = []
      for(let record of records){
        try{

          let field = await Field.findOne({ name: record['Field']}).exec()
          if(!field){
            field = new Field({ name: record['Field'] })
            field = await field.save()
          }
          let outreachCenter = new Programme({
            name  : record['Name'] ,
            field : field._id,
            level : record['Level']
          })
          await outreachCenter.save()
          success.push({ record: record })
        }catch(e){
          errors.push({ record: record, error:e })
        }
      }

      res.status(200).send({ success: success, errors: errors })
    }catch(e){
      res.status(200).send(e)
    }
  }

  add = async(req, res, next) => {
    try{

      const {name , field , level} = req.body

      let programme = new Programme({
        name      : name ,
        field : field ,
        level : level,
      })

      await programme.save()
      res.status(200).send({data: programme})
    }
    catch(e){
      next(e)
    }
  }


  update = async(req, res, next) => {
      try{

        const{  name , field , level,} = req.body
        const id = req.params.id
        let programme = await Programme.findOne({_id: id}).exec()

        let properties = Object.keys(req.body)
        properties.forEach((p)=>{
          programme[p] = req.body[p]
        })

        var saved =  await programme.save()
        res.status(200).send({data: saved})
      }
      catch(e){
        next(e)
      }
  }


  downloadExample = async (req, res,) => {
    try{
      let filePath = path.join(__dirname, '../../extra_files/programmes.csv')
      res.sendFile(filePath)
    }catch(e){
      throw e
    }
  }





  // delete = async(req, res, next) => {
  //   try{
  //     const id = req.params.id
  //     await Programme.findByIdAndUpdate(id, {status: Deleted})
  //     res.status(200).send()
  //   }catch(e){
  //     res.status(500).send()
  //   }
  // }



}

export default ProgrammeController
