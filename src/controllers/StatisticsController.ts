import { IStatistics, ProgrammeStats } from '../models/Statistics'
import Statistics from '../models/Statistics'
import { parse } from 'csv-parse'
//@ts-ignore
import { getFileType } from '../models/File'
import InstitutionProgrammes from '../models/InstitutionProgrammes'
import Institution from '../models/Institution'
import OutreachCenter from '../models/OutreachCenter'
import Campus from '../models/Campus'
import Programme from '../models/Programme'
import { stringify } from 'csv-stringify';
import { generate } from 'csv-generate';
import { exception } from 'console'
var Readable = require('stream').Readable


class StatisticsController {

  constructor() {

  }


  ////db.statistics.createIndex({name: "text", identityNumber: "text", flats:"text", role:"text"})
  //all = async (req, res, next) => {
  //  try{
  //    const page = req.query.page ? parseInt(req.query.page) : 1
  //    const limit = req.query.limit ? parseInt(req.query.limit) : 20
  //    let skip = page > 1 ? limit*(page-1) : 0
  //    const textQuery = req.query.textQuery

  //    let regexQ = new RegExp(textQuery, 'i')
  //    let findObj = {}
  //    if(textQuery)
  //      findObj['$or'] = [{name: regexQ},  {uniqueNumber: regexQ}, {email: regexQ}]

  //    const statistics = await Statistics.find(findObj, null, {skip:skip, limit:limit}).exec()
  //    let statisticsCount = await Statistics.countDocuments(findObj)
  //    let statisticsPaginated = new PaginatedModel(statistics, statisticsCount, skip, limit)
  //    res.status(200).json(statisticsPaginated);
  //  }catch(e){
  //    res.status(500).send(e)
  //  }
  // }

//   progress = async ( req, res, next ) =>{
//     try{
//       const agg =   Statistics.aggregate([
//         { $match: { relatedInstitution: mongoose.Types.ObjectId("61a70c30d5293a8fd92e054d") }, },
//         { "$addFields": { programmeStatsCompleted: { $allElementsTrue: [ "$programmeStats.completed" ] } }
//         },
//         {
//           $set: {
//             requiredToComplete: [ "$staff.female", "$staff.male", "$lecturers.fullTimeForeignMale", "$lecturers.fullTimeForeignFemale", "$lecturers.fullTimeLocalMale", "$lecturers.fullTimeLocalFemale", "$lecturers.partTimeForeignMale", "$lecturers.partTimeForeignFemale", "$lecturers.partTimeLocalMale", "$lecturers.partTimeLocalFemale", "$programmeStatsCompleted" ]
//           }
//         },
//         { $project:
//             {
//               requiredToComplete:
//                   { $function:
//                     {
//                         body: function(array) {
//                            return array.map((x)=>{
//                                if(x == 0)
//                                   return 1
//                               return x
//                            });
//                         },
//                         args: [ "$requiredToComplete" ],
//                         lang: "js"
//                     }
//                   },
//           },
//         },
//         {
//           "$project": {
//             completed: { $allElementsTrue: [ "$requiredToComplete" ] }
//           }
//         }
//       ])
//       let progress = await agg.exec()
//       res.status(200).send(progress)
//       // }
//
//     }catch(e){
//
//     }
//   }

  allNonPaged = async (req, res, next) => {
    try{
      const statistics = await Statistics.find({})
        .select('relatedInstitution institutionType completed completedPercent startDate endDate')
        .exec()
      res.status(200).send(statistics)
    }catch(e){
      throw e
    }
  }

  single = async (req, res, next) => {
    try {
      const statisticsId = req.params.id
      const type = req.params.type
      let statistics = await Statistics.findOne({ relatedInstitution: statisticsId, institutionType: type}).select('-programmeStats').exec()
      if (!statistics) {
        statistics = new Statistics({
          relatedInstitution : statisticsId,
          institutionType : type
        })
      }
      await statistics.save()
      res.status(200).json({
        data: statistics
      })
    } catch (error) {
      next(error)
    }
  }

  downloadCSVForStatistics = async (req, res, next) => {
    try{
      const { _id, relatedInstitution, institutionType } = req.params

      let statistics = await Statistics.findOne({relatedInstitution: relatedInstitution, institutionType: institutionType})


      let programmesNotInStats = statistics.programmeStats.sort((a,b)=>{
        if(a.programme.name > b.programme.name)
          return 1
        if(a.programme.name < b.programme.name)
          return -1
        return 0
      })

      let csv = stringify(programmesNotInStats, {
        header: true,
        columns: [
          {key: 'programme.name', header: 'Name' },
          { key: 'programme.level', header: 'Level' },
          { key: 'femaleStats.newStudents', header: 'Female New' },
          { key: 'femaleStats.oldStudents', header: 'Female Continuing' },
          { key: 'femaleStats.graduates', header: 'Female Graduates' },
          { key: 'femaleStats.dropouts', header: 'Female Dropouts' },
          { key: 'maleStats.newStudents', header: 'Male New' },
          { key: 'maleStats.oldStudents', header: 'Male Continuing' },
          { key: 'maleStats.graduates', header: 'Male Graduates' },
          { key: 'maleStats.dropouts', header: 'Male Dropouts' },
        ],
      })
      res.attachment('example.csv')
      csv.pipe(res)
      // res.status(200).send(csv)
    }catch(e){
      throw e
    }
  }

  downloadCSVForProgrammeAddition = async (req, res, next) => {
    try{
      const { _id, relatedInstitution, institutionType } = req.params

      let allProgrammes = await Programme.find({}).exec()

      let statistics = await Statistics.findOne({relatedInstitution: relatedInstitution, institutionType: institutionType})

      let programmesInStatistics = []
      if(statistics)
        programmesInStatistics = statistics.programmeStats.map(p => p.programme._id)

      let programmesNotInStats = allProgrammes.filter(p => !programmesInStatistics.includes(p._id)).sort((a,b)=>{
        if(a.name > b.name)
          return 1
        if(a.name < b.name)
          return -1
        return 0
      })

      let csv = stringify(programmesNotInStats, {
        header: true,
        columns: [ {key: 'name', header: 'Name' }, { key: 'level', header: 'Level' } ],
      })
      res.attachment('example.csv')
      csv.pipe(res)
      // res.status(200).send(csv)
    }catch(e){
      throw e
    }
  }

  uploadProgrammes = async (req, res, next) => {
    try{
      if(!req.files || !req.files.files || Object.keys(req.files.files).length === 0)
        return res.status(400).send('No files uploaded')
      let { institutionType, relatedInstitution } = req.params
      let currentFile = null

      if(Array.isArray(req.files.files)){
        currentFile = req.files.file[0]
      }else{
        currentFile = req.files.files
      }

      // let mimeType = getFileType(currentFile.mimetype)
      // if(mimeType != 'csv')
      //   return res.status(500).send('Wrong file type')

      let institution = null
      switch (institutionType) {
        case 'Campus':
          institution = await Campus.findOne({_id : relatedInstitution})
          break;
        case 'OutreachCenter':
          institution = await OutreachCenter.findOne({_id : relatedInstitution})
          break;
        case 'Institution':
          institution = await Institution.findOne({_id : relatedInstitution})
          break;
        default:
          return res.status(500).send('Institution Type given is wrong or not given.')
      }
      if(!institution)
        return res.status(500).send('Institution given is wrong.')


      let statistics = await Statistics.findOne({ relatedInstitution: relatedInstitution, institutionType: institutionType })
      if(!statistics){
        statistics = new Statistics({
          relatedInstitution: relatedInstitution,
          institutionType: institutionType
        })
      }
      await statistics.save()

      var s = new Readable()
      s.push(currentFile.data)
      s.push(null)

      let recordsParser = s.pipe(parse({columns: true}))
      let records = []
      for await (const record of recordsParser) {
        records.push(record)
      }

      let errors = []
      let success = []



      for(let record of records){
        try{
          statistics = await Statistics.findOne({ relatedInstitution: relatedInstitution, institutionType: institutionType })
          let currentProgrammeStatsIds = statistics.programmeStats.map(a => a.programme._id)

          // console.log('statistics', statistics)
          let programme = await Programme.findOne({ name: record['Name'], level: record['Level']}).exec()
          if(!programme)
            throw exception(`Programme does not exist, ${record['Name']}, ${record['Level']}`)

          // console.log('programme', programme)
          //@ts-ignore
          let programmeInStats = currentProgrammeStatsIds.includes(programme._id)

          if(programmeInStats){
            continue;
          }

          statistics.programmeStats.push({
            programme   : programme._id as any,
            maleStats   : null,
            femaleStats : null,
            completed   : false,
          })
          await statistics.save()
          success.push({ record: record })
        }catch(e){
          errors.push({ record: record, error:e })
        }
      }
      this.saveStatisticsCondition(relatedInstitution, institutionType)

      res.status(200).send({ success: success, errors: errors })
    }catch(e){
      throw e
    }
  }

  saveDates = async (req, res, next) => {
    try {

      //if new has to give newType
      const {  relatedInstitution, institutionType , startDate, endDate } = req.body
      if(!endDate)
        return res.status(500).send('An end date must be given.')

      var statistics: IStatistics = null

      if (relatedInstitution)
        statistics = await Statistics.findOne({ relatedInstitution: relatedInstitution, institutionType: institutionType })
      if (statistics == null)
        statistics = new Statistics()

      let properties = Object.keys(req.body)
      properties.forEach((p) => {
        statistics[p] = req.body[p]
      })

      await statistics.save()

      res.status(200).send({ data: statistics })
    }
    catch (e) {
      next(e)
    }
  }

  save = async (req, res, next) => {
    try {

      //if new has to give newType
      const { _id, relatedInstitution, institutionType } = req.body

      var statistics: IStatistics = null

      if (relatedInstitution)
        statistics = await Statistics.findOne({ relatedInstitution: relatedInstitution, institutionType: institutionType })
      if (statistics == null)
        statistics = new Statistics()

      let properties = Object.keys(req.body)
      properties.forEach((p) => {
        statistics[p] = req.body[p]
      })

      await statistics.save()

      this.saveStatisticsCondition(relatedInstitution,institutionType)

      res.status(200).send({ data: statistics })
    }
    catch (e) {
      next(e)
    }
  }

  getAllProgrammes = async (req, res, next) => {
    try {
      const { relatedInstitution, institutionType } = req.params

      let statistics = await Statistics.findOne({ relatedInstitution: relatedInstitution, institutionType: institutionType }).select('programmeStats.programme programmeStats.completed')

      // let programmes = statistics.programmeStats.map((p) => {
      //   return p
      // })

      if (!statistics){
        statistics = new Statistics({ relatedInstitution: relatedInstitution, institutionType:institutionType, programmeStats: [] })
        await statistics.save()
      }

      return res.status(200).send(statistics.programmeStats)

    } catch (e) {
      return res.status(500).send(e)
    }
  }

  getProgramme = async (req, res, next) => {
    try {
      const { relatedInstitution, institutionType, programme } = req.params

      let statistics = await Statistics.findOne({ relatedInstitution: relatedInstitution, institutionType: institutionType })

      if (!statistics)
        return res.status(300).send('The statistics does not exist')

      let programmeStats = statistics.programmeStats.find(p => p.programme == programme || p.programme._id == programme)

      if (!programmeStats)
        return res.status(300).send('This programme has not been added to statistics yet.')

      return res.status(200).send(programmeStats)
    } catch (e) {
      return res.status(500).send(e)
    }
  }

  removeProgramme = async (req, res, next) => {
    try{
      const { _id, relatedInstitution, institutionType, programme} = req.body

      let statistics = await Statistics.findOne({ relatedInstitution: relatedInstitution, institutionType: institutionType })

      if (!statistics)
        return res.status(500).send('Wrong statistics id.')

      if (!statistics.programmeStats)
        return res.status(200).send()

      statistics.programmeStats = statistics.programmeStats.filter( p => p.programme._id != programme && p.programme != programme )
      await statistics.save()
      this.saveStatisticsCondition(relatedInstitution, institutionType)

      res.status(200).send()
    }catch(e){
      throw e
    }
  }

  saveProgramme = async (req, res, next) => {
    try {

      //if new has to give newType
      const { _id, relatedInstitution, institutionType, programme, maleStats, femaleStats } = req.body

      var statistics: IStatistics = null

      statistics = await Statistics.findOne({ relatedInstitution: relatedInstitution, institutionType: institutionType })

      if (!statistics)
        statistics = new Statistics({ relatedInstitution: relatedInstitution, institutionType: institutionType })

      if (!statistics.programmeStats)
        statistics['programmeStats'] = []

      let found = false

      statistics.programmeStats = statistics.programmeStats.map((ps) => {
        if (ps.programme == programme || ps.programme._id == programme) {
          ps['maleStats'] = maleStats
          ps['femaleStats'] = femaleStats
          if(
            maleStats != null &&
            femaleStats != null &&
            maleStats.newStudents   != null &&
            maleStats.oldStudents   != null &&
            maleStats.dropouts      != null &&
            maleStats.graduates     != null &&
            femaleStats.newStudents != null &&
            femaleStats.oldStudents != null &&
            femaleStats.dropouts    != null &&
            femaleStats.graduates   != null
          )
            ps['completed'] = true
          else
            ps['completed'] = false
          found = true
        }
        return ps
      })


      if (found == false) {
        statistics.programmeStats.push({
          programme: programme,
          maleStats: maleStats,
          femaleStats: femaleStats,
          completed : false,
        })
      }

      this.saveStatisticsCondition(relatedInstitution,institutionType)


      await statistics.save()

      let savedProgramme = statistics.programmeStats.find((p)=>{ return p.programme._id == programme })

      res.status(200).send({ data: savedProgramme })
    }
    catch (e) {
      next(e)
    }
  }

  uploadStatistics = async (req, res, next) => {
    try{
      let { relatedInstitution, institutionType }  = req.params
      if(!req.files || !req.files.files || Object.keys(req.files.files).length === 0)
        return res.status(400).send('No files uploaded')

      let currentFile = null

      if(Array.isArray(req.files.files)){
        currentFile = req.files.file[0]
      }else{
        currentFile = req.files.files
      }

      // let mimeType = getFileType(currentFile.mimetype)
      // if(mimeType != 'csv')
      //   return res.status(500).send('Wrong file type')

      let institution = null
      switch (institutionType) {
        case 'Campus':
          institution = await Campus.findOne({_id : relatedInstitution})
          break;
        case 'OutreachCenter':
          institution = await OutreachCenter.findOne({_id : relatedInstitution})
          break;
        case 'Institution':
          institution = await Institution.findOne({_id : relatedInstitution})
          break;
        default:
          return res.status(500).send('Institution Type given is wrong or not given.')
      }
      if(!institution)
        return res.status(500).send('Institution given is wrong.')


      let statistics = await Statistics.findOne({ relatedInstitution: relatedInstitution, institutionType: institutionType })
      if(!statistics){
        statistics = new Statistics({
          relatedInstitution: relatedInstitution,
          institutionType: institutionType
        })
      }
      await statistics.save()

      var s = new Readable()
      s.push(currentFile.data)
      s.push(null)

      let recordsParser = s.pipe(parse({columns: true}))
      let records = []
      for await (const record of recordsParser) {
        records.push(record)
      }

      let errors = []
      let success = []

      let recordsMapped = records.map((record)=>{
        return {

        }
      })

      for(let record of records){
        try{
          statistics = await Statistics.findOne({ relatedInstitution: relatedInstitution, institutionType: institutionType })

          let programme = await Programme.findOne({ name: record['Name'], level: record['Level']}).exec()
          if(!programme)
            throw exception(`Programme does not exist, ${record['Name']}, ${record['Level']}`)

          let programmeInStats = statistics.programmeStats.find(p => p.programme.name == record['Name'] && p.programme.level == record['Level'])

          let maleStats = {
            newStudents : record['Male New'],
            oldStudents : record['Male Continuing'],
            dropouts    : record['Male Dropouts'],
            graduates   : record['Male Graduates'],
          }
          let femaleStats = {
            newStudents : record['Female New'] ,
            oldStudents : record['Female Continuing'] ,
            dropouts    : record['Female Dropouts'] ,
            graduates   : record['Female Graduates'] ,
          }
          let completed = false
          let badValues = [ null, undefined, '' ]
          if(
            maleStats != null &&
            femaleStats != null &&
            !badValues.includes(maleStats.newStudents)    &&
            !badValues.includes(maleStats.oldStudents)    &&
            !badValues.includes(maleStats.dropouts)       &&
            !badValues.includes(maleStats.graduates)      &&
            !badValues.includes(femaleStats.newStudents)  &&
            !badValues.includes(femaleStats.oldStudents)  &&
            !badValues.includes(femaleStats.dropouts)     &&
            !badValues.includes(femaleStats.graduates)
          )
            completed = true
          else
            completed = false

          if(programmeInStats){
            programmeInStats.maleStats = maleStats
            programmeInStats.femaleStats = femaleStats
            programmeInStats.completed = completed
          }
          else{
            statistics.programmeStats.push({
              programme   : programme._id as any,
              maleStats   : maleStats,
              femaleStats : femaleStats,
              completed   : completed,
            })
          }

          await statistics.save()
          success.push({ record: record })
        }catch(e){
          errors.push({ record: record, error:e })
        }
      }
      this.saveStatisticsCondition(relatedInstitution, institutionType)

      res.status(200).send({ success: success, errors: errors })
        res.status(200).send()
      res.status(200).send()
    }catch(e){
      throw e
    }
  }

  saveStatisticsCondition = async (heiId, heiType ) => {
    try{

      let hei = null
      if(heiType  == 'Institution')
        hei = await Institution.findById(heiId)
      if(heiType  == 'OutreachCenter')
        hei = await OutreachCenter.findById(heiId)
      if(heiType  == 'Campus')
        hei = await Campus.findById(heiId)

      if(!hei)
        throw 'HEI not found'
      let statistics = await Statistics.findOne({ relatedInstitution: heiId, institutionType: heiType })
      if(!statistics)
        throw 'Statistics not found'

      let completed = true
      let programmesCompleted = true
      let totalProgrammes = statistics.programmeStats.length
      let completedProgrammes = statistics.programmeStats.filter(p => p.completed == true).length
      let completedPercent = Math.round(completedProgrammes/totalProgrammes*100)
      if(completedPercent < 100)
        completed = false
      if(programmesCompleted == true){
        if(
          statistics?.staff?.female !=null &&
          statistics?.staff?.male !=null &&
          statistics?.lecturers?.partTimeForeignMale !=null &&
          statistics?.lecturers?.partTimeForeignFemale !=null &&
          statistics?.lecturers?.partTimeLocalMale !=null &&
          statistics?.lecturers?.partTimeLocalFemale !=null &&
          statistics?.lecturers?.fullTimeForeignMale !=null &&
          statistics?.lecturers?.fullTimeForeignFemale !=null &&
          statistics?.lecturers?.partTimeForeignMale !=null &&
          statistics?.lecturers?.partTimeForeignFemale != null
        ){
          completed = true
        }
      }

      statistics.completed = completed
      statistics.completedPercent = completedPercent
      hei.statsComplete = completed
      hei.completedPercent = completedPercent
      hei.save()
      statistics.save()

    }catch(e){
      throw e
    }
  }
}

export default StatisticsController
