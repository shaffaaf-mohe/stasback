import PaginatedModel from '../models/PaginatedModel'
import MainField from '../models/MainField'
import {parse} from 'csv-parse'
//@ts-ignore
import { getFileType } from '../models/File'
import path from 'path'
var Readable = require('stream').Readable


class MainFieldController
{

  constructor(){

  }


  //db.object.createIndex({name: "text", identityNumber: "text", flats:"text", role:"text"})
  all = async (req, res, next) => {
    try{
      const page = req.query.page ? parseInt(req.query.page) : 1
      const limit = req.query.limit ? parseInt(req.query.limit) : 20
      let skip = page > 1 ? limit*(page-1) : 0
      const textQuery = req.query.textQuery

      let regexQ = new RegExp(textQuery, 'i')
      let findObj = {}
      if(textQuery)
        findObj['$or'] = [{name: regexQ},  {uniqueNumber: regexQ}, {email: regexQ}]

      const object = await MainField.find(findObj, null, {skip:skip, limit:limit}).sort({ name: 1 }).exec()
      let objectCount = await MainField.countDocuments(findObj)
      let objectPaginated = new PaginatedModel(object, objectCount, skip, limit)
      res.status(200).json(objectPaginated);
    }catch(e){
      res.status(500).send(e)
    }
   }



  single = async (req, res, next) => {
    try {
      const objectId = req.params.id
      const object = await MainField.findById(objectId).exec()
      if (!object) return next(new Error('MainField does not exist'));
      res.status(200).json({
        data: object
      })
      } catch (error) {
      next(error)
    }
  }


  uploadData = async (req, res, next) => {
    try{
      if(!req.files || !req.files.files || Object.keys(req.files.files).length === 0)
        return res.status(400).send('No files uploaded')

      let currentFile = null

      if(Array.isArray(req.files.files)){
        currentFile = req.files.file[0]
      }else{
        currentFile = req.files.files
      }

      // let mimeType = getFileType(currentFile.mimetype)
      // if(mimeType != 'csv')
      //   return res.status(500).send('Wrong file type')

      var s = new Readable()
      s.push(currentFile.data)
      s.push(null)

      let recordsParser = s.pipe(parse({columns: true}))
      let records = []
      for await (const record of recordsParser) {
        records.push(record)
      }

      let errors = []
      let success = []
      for(let record of records){
        try{

          let mainfield = new MainField({
            name  : record['Name'] ,
          })
          await mainfield.save()
          success.push({ record: record })
        }catch(e){
          errors.push({ record: record, error:e })
        }
      }

      res.status(200).send({ success: success, errors: errors })
    }catch(e){
      res.status(200).send(e)
    }
  }

  add = async(req, res, next) => {
    try{
      const {name} = req.body
      let object = new MainField({name : name })
      await object.save()
      res.status(200).send({data: object})
    }
    catch(e){
      next(e)
    }
  }


  update = async(req, res, next) => {
      try{

        const{  name } = req.body
        const id = req.params.id
        let object = await MainField.findOne({_id: id}).exec()

        let properties = Object.keys(req.body)
        properties.forEach((p)=>{
          object[p] = req.body[p]
        })

        var saved =  await object.save()
        res.status(200).send({data: saved})
      }
      catch(e){
        next(e)
      }
  }



  downloadExample = async (req, res,) => {
    try{
      let filePath = path.join(__dirname, '../../extra_files/mainfields.csv')
      res.sendFile(filePath)
    }catch(e){
      throw e
    }
  }



  // delete = async(req, res, next) => {
  //   try{
  //     const id = req.params.id
  //     await object.findByIdAndUpdate(id, {status: Deleted})
  //     res.status(200).send()
  //   }catch(e){
  //     res.status(500).send()
  //   }
  // }



}

export default MainFieldController
