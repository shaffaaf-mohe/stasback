//import PaginatedModel from '../models/PaginatedModel'
//import InstitutionProgrammes from '../models/InstitutionProgrammes'
//import {parse} from 'csv-parse'


//class ProgrammeController
//{

//  constructor(){

//  }


//  //db.programme.createIndex({name: "text", identityNumber: "text", flats:"text", role:"text"})
//  all = async (req, res, next) => {
//    try{
//      const page = req.query.page ? parseInt(req.query.page) : 1
//      const limit = req.query.limit ? parseInt(req.query.limit) : 20
//      let skip = page > 1 ? limit*(page-1) : 0
//      const textQuery = req.query.textQuery
//      const { institution, campus, outreachCenter } = req.params

//      let regexQ = new RegExp(textQuery, 'i')
//      let findObj = {}
//      if(textQuery)
//        findObj['$or'] = [{name: regexQ},  {uniqueNumber: regexQ}, {email: regexQ}]

//      const programme = await InstitutionProgrammes.find(findObj, null, {skip:skip, limit:limit}).exec()
//      let programmeCount = await InstitutionProgrammes.countDocuments(findObj)
//      let programmePaginated = new PaginatedModel(programme, programmeCount, skip, limit)
//      res.status(200).json(programmePaginated);
//    }catch(e){
//      res.status(500).send(e)
//    }
//   }



//  single = async (req, res, next) => {
//    try {
//      const relatedInstitution = req.params.id
//      const programme = await InstitutionProgrammes.findOne({ relatedInstitution: relatedInstitution }).exec()
//      if (!programme) return next(new Error('Programme does not exist'));
//      res.status(200).json({
//        data: programme
//      })
//      } catch (error) {
//      next(error)
//    }
//  }


//  add = async(req, res, next) => {
//    try{

//      const {  name , field , level, relatedInstitution, institutionType } = req.body

//      let institutionProgrammes = new InstitutionProgrammes( {programmes: [],  institutionType: institutionType, relatedInstitution: relatedInstitution })

//      let existingInstitutionProgrammes = null
//      if(relatedInstitution)
//        existingInstitutionProgrammes = await InstitutionProgrammes.findOne({ relatedInstitution: relatedInstitution}).exec()
//      console.log('existingInstitutionProgrammes', existingInstitutionProgrammes )
//      if(existingInstitutionProgrammes)
//        institutionProgrammes = existingInstitutionProgrammes

//      let programme = {
//        name      : name as string,
//        field     : field as string,
//        level     : level as string,
//      }
//      programme['field'] = field
//      programme.field = 'something'
//      console.log('field', field)
//      console.log('programme', programme)
//      let existingProgramme = institutionProgrammes.programmes.find( p => p.name == name && p.level == level )

//      if(existingProgramme)
//        return res.status(400).send('This programme already exists')

//      //@ts-ignore
//      institutionProgrammes.programmes.push(programme)
//      console.log('institutionProgrammes', institutionProgrammes)
//      await institutionProgrammes.save()

//      res.status(200).send({data: institutionProgrammes})
//    }
//    catch(e){
//      next(e)
//    }
//  }


//  remove = async(req, res, next) => {
//    try{

//      const { _id, name , field , level, relatedInstitution, institutionType } = req.body
//      console.log('req.body', req.body)
//      let institutionProgrammes = await InstitutionProgrammes.findOne ({ relatedInstitution : relatedInstitution}).exec()


//      let existingProgramme = institutionProgrammes.programmes.filter( p => p.name == name && p.level == level )

//      if(!existingProgramme)
//        return res.status(400).send('This programme does not exist.')

//      // todo
//      institutionProgrammes.programmes = institutionProgrammes.programmes.filter((p) => {
//        if(p.name == name && p.level == level)
//          return false
//        return true
//      })
//      await institutionProgrammes.save()

//      res.status(200).send({data: institutionProgrammes})
//    }
//    catch(e){
//      next(e)
//    }
//  }







//  // delete = async(req, res, next) => {
//  //   try{
//  //     const id = req.params.id
//  //     await Programme.findByIdAndUpdate(id, {status: Deleted})
//  //     res.status(200).send()
//  //   }catch(e){
//  //     res.status(500).send()
//  //   }
//  // }



//}

//export default ProgrammeController
