
import * as express from 'express'
import { getuid, send } from 'process';
// const User = require('../models/userModel');
import User, {UserType, UserStatus, IUser} from '../models/User'
import { v4 as uuidv4 } from 'uuid';
import nodeMailer from 'nodemailer'
import MailService from '../services/mailservice'

const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');


export async function hashPassword(password) {
  return await bcrypt.hash(password, 10);
}

export async function validatePassword(plainPassword, hashedPassword) {
  return await bcrypt.compare(plainPassword, hashedPassword);
}

export async function ensureHEMSStaff(){
  try{
    let user = await User.findOne({ email: 'staff@mohe.gov.mv' }).exec()
    if(user) return
    user = new User({
      name: 'Staff',
      type: UserType.HEMStaff,
      email: 'staff@mohe.gov.mv',
      password: await hashPassword('mohestaffhere'),
      status: UserStatus.Active,
      inviteId: await uuidv4(),
    })
    await user.save()
  } catch(e) {
    console.log('e', e)
  }
}

export  async function ensureAdmin(){
  try{
    let user = await User.findOne({ email: 'admin@mohe.gov.mv' }).exec()
    if(user) return
    user = new User({
      name: 'Admin',
      type: UserType.Admin,
      email: 'admin@mohe.gov.mv',
      password: await hashPassword('adminishere'),
      status: UserStatus.Active,
      inviteId: await uuidv4(),
    })
    await user.save()
  } catch(e) {
    console.log('e', e)
  }
}

class AuthController
{

  constructor(){

  }


  register = async (req, res, next) => {
    try {
      const {  password, name, email,  } = req.body

      /* console.log(password) */
      const uuid = uuidv4()
      const hashedPassword = await hashPassword(password);
      const newUser = new User({
        email, password: hashedPassword,  name
      })
      const accessToken = jwt.sign({ userId: newUser._id }, process.env.JWT_SECRET, {
        expiresIn: "1d"
      });
      newUser.accessToken = accessToken

      await newUser.save()
      delete newUser.password
      delete newUser.userConfirmation


      res.json({
        data: newUser,
        message: "You have signed up successfully, an email has been sent for verification"
      })
    } catch (error) {
      next(error)
    }
  }



  sendConfirmationEmail(newUser : IUser){
    try{
      let mailer = new MailService()
      let to = newUser.email
      let subject = "Please confirm your registration to the raincrest app"
      let html =  `<p>Dear ${newUser.name}</p><p>Please click the link below to confirm your Raincrest app account</p><p><a href="${process.env.BASE_URL}/api/users/activate/email/${newUser.userConfirmation.emailConfirmString}"> LINK TO CONFIRMATION </a></p>`
      let text = `Dear ${newUser.name}, Please click the link to verify your account. Link ${process.env.BASE_URL}/api/user/activate/email/${newUser.userConfirmation.emailConfirmString}`

      mailer.sendEmail(to, subject, html, text)
    }catch(e){
      throw(e)
    }

  }

  resendEmail = async(req,res,next) => {
    console.log('inside now ', )
    try{
      const user = req.user
      this.sendConfirmationEmail(user)
      res.status(200).send('Confirmation email has been resent')
    }catch(e){
      res.status(500).send(e)
    }
  }

  login = async (req, res, next) => {
    try {
      const { email, password } = req.body;
      const user = await User.findOne({ email });
      console.log('user', user)
      if (!user) return next(new Error('User does not exist'));
      const validPassword = await validatePassword(password, user.password);
      if (!validPassword) return next(new Error('Password is not correct'))
      const accessToken = jwt.sign({ userId: user._id }, process.env.JWT_SECRET, {
        expiresIn: "20h"
      });
      await User.findByIdAndUpdate(user._id, { accessToken })
      res.status(200).json({
        user: {id:user._id,  email: user.email, name:user.name, type: user.type, status: user.status, heiType: user.heiType, relatedHEI: user.relatedHEI},
        accessToken
      })
    } catch (error) {
      next(error);
    }
  }


  checkAuth = async (req, res, next) => {
    // console.log('in check Auth', )
    res.status(200).send('Valid')
  }

  checkSomething = async (req,res,next) =>{
    try{
      res.status(200).send('success')
    } catch(e) {
      res.status(500).send({e:e})
    }
  }

}

export default AuthController
