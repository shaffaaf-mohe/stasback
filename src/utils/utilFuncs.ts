interface dispValue {
  display: string
  value: any
}

export const StringIsNumber = value => isNaN(Number(value)) === false;


export function enumToArray(enumme){
  // console.log('enum',enumme)
  try{
    return Object.keys(enumme)
      .map(key => enumme[key]);
  }catch(e){
    return e
  }
}

export function enumToDispValues(enumme): Array<dispValue>{
  return Object.entries(enumme)
    .map(([k,v]) => {return {display: k, value: v}} )
}