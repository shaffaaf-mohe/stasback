1. Query for checking if institution has completed statistics
``` javascript
db.statistics.aggregate([
  {
    $match: {
      relatedInstitution: ObjectId("61a70c30d5293a8fd92e054d")
    },

  },
  {
    "$addFields": {
      programmeStatsCompleted: {
        $allElementsTrue: [
          "$programmeStats.completed"
        ]
      }
    }
  },
  {
    $set: {
      requiredToComplete: [
        "$staff.female",
        "$staff.male",
        "$lecturers.fullTimeForeignMale",
        "$lecturers.fullTimeForeignFemale",
        "$lecturers.fullTimeLocalMale",
        "$lecturers.fullTimeLocalFemale",
        "$lecturers.partTimeForeignMale",
        "$lecturers.partTimeForeignFemale",
        "$lecturers.partTimeLocalMale",
        "$lecturers.partTimeLocalFemale",
        "$programmeStatsCompleted"
      ]
    }
  },
  { $project:
      {
        requiredToComplete:
            { $function:
              {
                  body: function(array) {
                     return array.map((x)=>{
                         if(x == 0)
                            return 1
                        return x
                     });
                  },
                  args: [ "$requiredToComplete" ],
                  lang: "js"
              }
            },

    },

  },
  {
    "$project": {
      requiredToComplete: 1,
      completed: {
        $allElementsTrue: [
          "$requiredToComplete"
        ]
      }
    }
  }
])
```

